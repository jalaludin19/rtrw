-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 22 Okt 2018 pada 14.27
-- Versi Server: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rtrw_prod`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `govdcmnt`
--

CREATE TABLE IF NOT EXISTS `govdcmnt` (
  `GDGOVID` varchar(12) NOT NULL,
  `GDTITL` varchar(100) NOT NULL,
  `GDFLNM` varchar(100) NOT NULL,
  `GDFLSZ` decimal(10,0) NOT NULL,
  `GDCATID` int(11) NOT NULL,
  `GDNOTE` varchar(250) NOT NULL,
  `GDUPDT` datetime NOT NULL,
  `GDUSID` varchar(25) NOT NULL,
  `GDSTAT` int(2) NOT NULL,
`GDROWID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `govdcmnt`
--
ALTER TABLE `govdcmnt`
 ADD PRIMARY KEY (`GDROWID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `govdcmnt`
--
ALTER TABLE `govdcmnt`
MODIFY `GDROWID` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
