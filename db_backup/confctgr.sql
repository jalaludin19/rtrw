-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 11 Feb 2019 pada 13.10
-- Versi Server: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rtrw_prod`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `confctgr`
--

CREATE TABLE IF NOT EXISTS `confctgr` (
  `cbentid` int(12) NOT NULL COMMENT 'entity id',
  `cbappid` varchar(6) NOT NULL COMMENT 'application id',
  `cbstco` varchar(10) NOT NULL COMMENT 'header code',
  `cbstky` varchar(10) NOT NULL COMMENT 'child code',
  `cbtext` varchar(50) NOT NULL COMMENT 'short desc',
  `cbdesc` text NOT NULL COMMENT 'long desc',
  `cbstat` smallint(6) NOT NULL COMMENT 'status',
  `cbupdt` datetime NOT NULL COMMENT 'update date',
  `cbusid` varchar(25) NOT NULL COMMENT 'update user',
`cbuuid` int(11) NOT NULL COMMENT 'row id'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data untuk tabel `confctgr`
--

INSERT INTO `confctgr` (`cbentid`, `cbappid`, `cbstco`, `cbstky`, `cbtext`, `cbdesc`, `cbstat`, `cbupdt`, `cbusid`, `cbuuid`) VALUES
(1, 'RTRW', 'JABATN', '', 'Sekertaris1', 'Sekertaris RT 2', 10, '2019-01-21 13:05:49', 'ADMIN', 2),
(1, 'RTRW', 'JABATN', 'JB00001', 'Kepala Trantib', 'Kepala Trantib', 10, '2018-10-08 00:00:00', 'ADMIN', 3),
(1, 'RTRW', 'KATDOK', 'KD00001', 'Rapat Bulanan ', 'Rapat Bulanan ', 10, '2018-12-24 13:09:30', 'ADMIN', 4),
(1, 'RTRW', 'JABATN', 'JB0001\n', 'aaaa', 'aaaaa', 10, '2019-01-06 16:49:08', 'ADMIN', 8);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `confctgr`
--
ALTER TABLE `confctgr`
 ADD PRIMARY KEY (`cbuuid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `confctgr`
--
ALTER TABLE `confctgr`
MODIFY `cbuuid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'row id',AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
