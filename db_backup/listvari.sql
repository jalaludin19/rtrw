-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 21, 2019 at 12:14 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rtrw_prod`
--

-- --------------------------------------------------------

--
-- Table structure for table `listvari`
--

DROP TABLE IF EXISTS `listvari`;
CREATE TABLE IF NOT EXISTS `listvari` (
  `lsgovid` varchar(12) NOT NULL,
  `lslstid` smallint(5) NOT NULL,
  `lslname` varchar(100) NOT NULL,
  `lsdescr` varchar(200) DEFAULT NULL,
  `lsstat` int(11) NOT NULL,
  `lsupdt` datetime DEFAULT NULL,
  `lsusid` varchar(25) DEFAULT NULL,
  `lsuuid` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`lsuuid`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `listvari`
--

INSERT INTO `listvari` (`lsgovid`, `lslstid`, `lslname`, `lsdescr`, `lsstat`, `lsupdt`, `lsusid`, `lsuuid`) VALUES
('RT0001', 1, 'Test 1', 'Test Descr Test1', 10, '2019-01-21 17:09:13', 'ADMIN', 2),
('RT0001', 2, 'Pedangang Keliling', 'Daftar Pedagang Keliling', 10, '2018-12-24 19:23:56', 'ADMIN', 3);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
