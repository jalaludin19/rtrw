-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 11, 2019 at 01:15 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 5.6.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rtrw_prod`
--

-- --------------------------------------------------------

--
-- Table structure for table `gvprofil`
--

CREATE TABLE `gvprofil` (
  `GPGOVID` int(12) NOT NULL,
  `GPPRVID` char(2) NOT NULL,
  `GPRGCID` char(4) NOT NULL,
  `GPDISID` char(7) NOT NULL,
  `GPVILID` char(10) NOT NULL,
  `GPGOVLV` varchar(3) NOT NULL,
  `GPRTNUM` varchar(4) NOT NULL,
  `GPRWNUM` varchar(4) NOT NULL,
  `GPADDRS` text NOT NULL,
  `GPPOSCD` varchar(6) NOT NULL,
  `GPPHONE` varchar(15) NOT NULL,
  `GPEMAIL` varchar(30) NOT NULL,
  `GPRTNM` varchar(100) NOT NULL,
  `GPRWNM` varchar(100) NOT NULL,
  `GPREGID` varchar(100) NOT NULL,
  `GPREGDT` datetime NOT NULL,
  `GPFLNM` varchar(100) NOT NULL,
  `GPUUID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gvprofil`
--

INSERT INTO `gvprofil` (`GPGOVID`, `GPPRVID`, `GPRGCID`, `GPDISID`, `GPVILID`, `GPGOVLV`, `GPRTNUM`, `GPRWNUM`, `GPADDRS`, `GPPOSCD`, `GPPHONE`, `GPEMAIL`, `GPRTNM`, `GPRWNM`, `GPREGID`, `GPREGDT`, `GPFLNM`, `GPUUID`) VALUES
(1, '31', '3172', '3172020', '3172020001', '2', '009', '06', 'Jl. Keruing 4 No.296 \r\nKelurahan Baktijaya ', '16418', '021-7704943', 'rinohenri@yahoo.com', 'Sartono', 'sartini ho', 'ADMIN', '2019-02-11 19:11:31', '680px-Coat_of_arms_of_Jakarta.svg.png', 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gvprofil`
--
ALTER TABLE `gvprofil`
  ADD PRIMARY KEY (`GPUUID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gvprofil`
--
ALTER TABLE `gvprofil`
  MODIFY `GPUUID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
