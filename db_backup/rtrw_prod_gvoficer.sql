-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: rtrw_prod
-- ------------------------------------------------------
-- Server version	5.7.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `gvoficer`
--

DROP TABLE IF EXISTS `gvoficer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gvoficer` (
  `OFGOVID` varchar(12) NOT NULL COMMENT 'Government ID',
  `OFOFFID` varchar(6) NOT NULL COMMENT 'Petugas ID',
  `OFPOSID` varchar(6) NOT NULL COMMENT 'Jabatan ID',
  `OFPRNIK` varchar(32) NOT NULL COMMENT 'NIK',
  `OFPRNAM` varchar(100) NOT NULL COMMENT 'Nama Petugas',
  `OFADDRS` text NOT NULL COMMENT 'Alamat',
  `OFCITY` varchar(100) NOT NULL COMMENT 'Kota',
  `OFDATFR` int(8) NOT NULL COMMENT 'Tanggal Mulai',
  `OFDATTO` int(8) DEFAULT NULL COMMENT 'Tanggal Berakhir',
  `OFACTIV` tinyint(4) NOT NULL COMMENT 'Active / Tidak',
  `OFSTAT` smallint(6) NOT NULL COMMENT 'Status',
  `OFUPDT` int(8) NOT NULL COMMENT 'Update Date',
  `OFUPTM` int(6) NOT NULL COMMENT 'Update Time',
  `OFUSID` varchar(25) NOT NULL COMMENT 'User Id',
  `OFUUID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'UUID',
  PRIMARY KEY (`OFUUID`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gvoficer`
--

LOCK TABLES `gvoficer` WRITE;
/*!40000 ALTER TABLE `gvoficer` DISABLE KEYS */;
INSERT INTO `gvoficer` VALUES ('RT0001','PG0004','1','1212','Petugas 4','dddd','Depok',20180507,NULL,1,10,20180514,113037,'ADMIN',35),('RT0001','PG0002','1','NIK091821','Petugas 2','Jl. Petugas 1 d','Depok',20180201,NULL,1,10,20180409,120544,'ADMIN',20),('RT0001','PG0003','1','NIK76111','Petugas 3','Jl. Petugas 3  a','Bogor',20180131,NULL,1,10,20180507,120707,'ADMIN',3);
/*!40000 ALTER TABLE `gvoficer` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-14 18:49:51
