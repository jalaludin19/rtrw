-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 12, 2018 at 01:43 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rtrw_prod`
--

-- --------------------------------------------------------

--
-- Table structure for table `gvancmt`
--

CREATE TABLE IF NOT EXISTS `gvancmt` (
`PMUUID` int(11) NOT NULL COMMENT 'AUTO ID',
  `PMGOVID` varchar(8) NOT NULL,
  `PMASTDT` date NOT NULL COMMENT 'START DATE',
  `PMSTIME` time NOT NULL COMMENT 'START TIME',
  `PMAENDT` date NOT NULL COMMENT 'END DATE',
  `PMETIME` time NOT NULL COMMENT 'END TIME',
  `PMASBJC` varchar(100) NOT NULL COMMENT 'SUBJECT PENGUMUMAN',
  `PMADESC` varchar(100) NOT NULL COMMENT 'DESCRIPTION',
  `PMASTAT` varchar(2) NOT NULL COMMENT 'STATUS',
  `PMARGDT` datetime NOT NULL COMMENT 'REGISTER DATE',
  `PMAUPDT` datetime NOT NULL COMMENT 'UPDATE DATE',
  `PMAUSID` varchar(10) NOT NULL COMMENT 'USER ID'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gvancmt`
--

INSERT INTO `gvancmt` (`PMUUID`, `PMGOVID`, `PMASTDT`, `PMSTIME`, `PMAENDT`, `PMETIME`, `PMASBJC`, `PMADESC`, `PMASTAT`, `PMARGDT`, `PMAUPDT`, `PMAUSID`) VALUES
(1, 'RT0001', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 'gggg', 'vvnnvhvjvhjjhhhhh', '20', '0000-00-00 00:00:00', '2018-07-23 00:00:00', 'ADMIN'),
(2, 'RT0001', '2018-10-08', '00:00:00', '2018-10-08', '00:00:00', 'test', 'test lagi', '20', '0000-00-00 00:00:00', '2018-08-06 00:00:00', 'ADMIN'),
(3, 'RT0001', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 'X', 'UGI', '20', '2018-09-03 19:31:12', '2018-09-03 19:31:12', 'ADMIN'),
(4, 'RT0001', '2018-09-03', '00:00:00', '2018-09-03', '00:00:00', 'X', 'UGI  2', '20', '2018-09-03 19:50:44', '2018-09-03 19:50:44', 'ADMIN'),
(5, 'RT0001', '2018-10-08', '00:00:00', '2018-10-11', '00:00:00', 'test', 'ghdjhdsjhdsj', '20', '2018-10-08 18:44:22', '2018-10-08 18:44:22', 'ADMIN'),
(6, 'RT0001', '2018-10-10', '00:00:00', '2018-10-13', '00:00:00', 'rrr', 'test', '20', '2018-10-08 19:03:54', '2018-10-08 19:03:54', 'ADMIN');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gvancmt`
--
ALTER TABLE `gvancmt`
 ADD KEY `PMMAUID` (`PMUUID`), ADD KEY `PMMAUID_2` (`PMUUID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gvancmt`
--
ALTER TABLE `gvancmt`
MODIFY `PMUUID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'AUTO ID',AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
