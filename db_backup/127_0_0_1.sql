-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 24, 2017 at 11:37 AM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `common_prod`
--
CREATE DATABASE IF NOT EXISTS `common_prod` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `common_prod`;

-- --------------------------------------------------------

--
-- Table structure for table `mstrenty`
--

DROP TABLE IF EXISTS `mstrenty`;
CREATE TABLE IF NOT EXISTS `mstrenty` (
  `MSENTID` varchar(6) NOT NULL,
  `MSTYPE` varchar(3) NOT NULL,
  PRIMARY KEY (`MSENTID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mstrenty`
--

INSERT INTO `mstrenty` (`MSENTID`, `MSTYPE`) VALUES
('RT0001', 'RT');
--
-- Database: `rtrw_prod`
--
CREATE DATABASE IF NOT EXISTS `rtrw_prod` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `rtrw_prod`;

-- --------------------------------------------------------

--
-- Table structure for table `govprfl`
--

DROP TABLE IF EXISTS `govprfl`;
CREATE TABLE IF NOT EXISTS `govprfl` (
  `GPGOVID` varchar(6) NOT NULL,
  `GPGOVNM` varchar(30) NOT NULL,
  `GPGOVLV` varchar(3) NOT NULL,
  `GPORGID` varchar(15) NOT NULL,
  `GPADDRS` text NOT NULL,
  `GPPOSCD` varchar(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `govprfl`
--

INSERT INTO `govprfl` (`GPGOVID`, `GPGOVNM`, `GPGOVLV`, `GPORGID`, `GPADDRS`, `GPPOSCD`) VALUES
('RT0001', '12', 'RT', '1111', 'test tost', '123456');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
