-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 21, 2019 at 12:14 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rtrw_prod`
--

-- --------------------------------------------------------

--
-- Table structure for table `listdata`
--

DROP TABLE IF EXISTS `listdata`;
CREATE TABLE IF NOT EXISTS `listdata` (
  `ldgovid` varchar(12) NOT NULL,
  `ldlstid` int(11) NOT NULL,
  `lddtid` int(11) NOT NULL,
  `ldcolid` int(11) NOT NULL,
  `ldtext` varchar(200) NOT NULL,
  `ldupdt` datetime DEFAULT NULL,
  `ldusid` varchar(25) DEFAULT NULL,
  `lduuid` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`lduuid`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `listdata`
--

INSERT INTO `listdata` (`ldgovid`, `ldlstid`, `lddtid`, `ldcolid`, `ldtext`, `ldupdt`, `ldusid`, `lduuid`) VALUES
('RT0001', 2, 1, 2, 'Soft', '2019-01-21 00:00:00', 'ADMIN', 42),
('RT0001', 1, 2, 1, 'Wk werk', '2018-10-29 00:00:00', 'ADMIN', 5),
('RT0001', 1, 2, 2, 'Kol 2b', '2018-10-29 00:00:00', 'ADMIN', 6),
('RT0001', 1, 2, 3, 'Sytle sheet', '2018-10-29 00:00:00', 'ADMIN', 7),
('RT0001', 2, 1, 1, 'AGG1', '2019-01-21 00:00:00', 'ADMIN', 41);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
