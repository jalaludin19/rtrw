-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 21, 2019 at 12:13 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rtrw_prod`
--

-- --------------------------------------------------------

--
-- Table structure for table `listcolm`
--

DROP TABLE IF EXISTS `listcolm`;
CREATE TABLE IF NOT EXISTS `listcolm` (
  `lcgovid` varchar(12) NOT NULL,
  `lclstid` int(11) NOT NULL,
  `lccolid` int(11) NOT NULL,
  `lccolnm` varchar(100) NOT NULL,
  `lcuuid` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`lcuuid`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `listcolm`
--

INSERT INTO `listcolm` (`lcgovid`, `lclstid`, `lccolid`, `lccolnm`, `lcuuid`) VALUES
('RT0001', 1, 2, 'Kol 2b', 3),
('RT0001', 1, 1, 'Kol 1a Test', 2),
('RT0001', 1, 3, 'Kolom 3', 4),
('RT0001', 2, 1, 'Nama Pedagang', 5),
('RT0001', 2, 2, 'Jenis Jualan', 6);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
