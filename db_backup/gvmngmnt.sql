-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 11, 2019 at 12:19 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rtrw_prod`
--

-- --------------------------------------------------------

--
-- Table structure for table `gvmngmnt`
--

DROP TABLE IF EXISTS `gvmngmnt`;
CREATE TABLE IF NOT EXISTS `gvmngmnt` (
  `MGGOVID` int(12) NOT NULL COMMENT 'Government ID',
  `MGOFFID` varchar(6) NOT NULL COMMENT 'Pengurus ID',
  `MGPOSID` varchar(10) NOT NULL COMMENT 'Jabatan ID',
  `MGPRNIK` varchar(32) NOT NULL COMMENT 'NIK',
  `MGPRNAM` varchar(100) NOT NULL COMMENT 'Nama Pengurus',
  `MGADDRS` text NOT NULL COMMENT 'Alamat',
  `MGCITY` varchar(100) NOT NULL COMMENT 'Kota',
  `MGDATFR` date NOT NULL COMMENT 'Tanggal Mulai',
  `MGDATTO` date DEFAULT NULL COMMENT 'Tanggal Berakhir',
  `MGACTIV` tinyint(4) NOT NULL COMMENT 'Active / Tidak',
  `MGSTAT` smallint(6) NOT NULL COMMENT 'Status',
  `MGUPDT` datetime NOT NULL COMMENT 'Update Date',
  `MGUSID` varchar(25) NOT NULL COMMENT 'User Id',
  `MGUUID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'UUID',
  PRIMARY KEY (`MGUUID`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gvmngmnt`
--

INSERT INTO `gvmngmnt` (`MGGOVID`, `MGOFFID`, `MGPOSID`, `MGPRNIK`, `MGPRNAM`, `MGADDRS`, `MGCITY`, `MGDATFR`, `MGDATTO`, `MGACTIV`, `MGSTAT`, `MGUPDT`, `MGUSID`, `MGUUID`) VALUES
(1, 'PG0001', 'JB00001', '12122', 'Doni', 'A2', 'Depok', '2019-02-11', '0000-00-00', 1, 10, '2019-02-11 18:06:38', 'ADMIN', 38);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
