$(function () {
	//$('#table-jabatan').dataTable();
});

function save() {
	$("#frmJabatan").submit();
}

function edit() {
	
}

function del() {
	if (confirm("Apakah anda yakin ingin hapus?")) {
		$( "#frmJabatan" ).submit();
	}
}

function delrecord(id) {
	if (confirm("Apakah anda yakin ingin hapus?")) {
		location.href="./jabatan/delete/" + id;
	}
}

function add(url) {
	location.href=url;
}

function back(url){
	location.href=url;
}