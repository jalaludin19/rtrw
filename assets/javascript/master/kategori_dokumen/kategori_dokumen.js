$(function () {
	//$('#table-kategori-dokumen').dataTable();
});

function save() {
	$("#frmKategoriDokumen").submit();
}

function edit() {
	
}

function del() {
	if (confirm("Apakah anda yakin ingin hapus?")) {
		$( "#frmKategoriDokumen" ).submit();
	}
}

function delrecord(id) {
	if (confirm("Apakah anda yakin ingin hapus?")) {
		location.href="./kategori_dokumen/delete/" + id;
	}
}

function add(url) {
	location.href=url;
}

function back(url){
	location.href=url;
}