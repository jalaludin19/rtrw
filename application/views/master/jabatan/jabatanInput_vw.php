<br>
<form class="form-horizontal" id="frmJabatan" method="post" action="<?php echo base_url()."master/jabatan/jabatan/".$formAction; ?>">
<input type="hidden" id="cbuuid" name="cbuuid" value="<?php echo $uuid;?>" >
<fieldset>
	<div class="form-group">
		<label class="col-md-2 control-label" for="cbtext">Nama Jabatan</label>  
	  	<div class="col-md-4">
	  		<input id="cbtext" name="cbtext" placeholder="Masukkan Nama Jabatan" class="form-control input-md" type="text" value="<?php echo (isset($dtljabatan) ? $dtljabatan[0]->cbtext:"");?>">	  
	  	</div>
	</div>
	<div class="form-group">
		<label class="col-md-2 control-label" for="cbdesc">Keterangan</label>  
		<div class="col-md-4">
		<textarea id="cbdesc" name="cbdesc" placeholder="Masukkan Keterangan" class="form-control" ><?php echo (isset($dtljabatan) ? $dtljabatan[0]->cbdesc:""); ?></textarea>  
		</div>
	</div>
</fieldset>
</form>

