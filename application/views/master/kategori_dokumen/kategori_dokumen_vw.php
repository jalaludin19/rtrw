<!-- tableview -->
<br><br>
<div class="table-responsive">
	<table class="table table-striped table-bordered table-hover" id="table-kategori-dokumen">
    	<thead>
    		<tr>
    			<td colspan="4"><a href='<?php echo base_url(); ?>master/kategori_dokumen/kategori_dokumen/add'>Tambah Kategori Dokumen</a></td>
    		</tr>
    		<tr>
    			<td>&nbsp;</td>
    			<td>Kategori Dokumen</td>
    			<td>Keterangan</td>
    			<td>&nbsp;</td>
    		</tr>
    	</thead>
    	<tbody>
		<?php 
		   $i=1;
    	   if (count($lstKatDok)>0) {
    	       foreach ($lstKatDok as $p) {
        	    echo "<tr>";
        	    echo "<td><input id='id".$i."' name='id[]' class='form-control' type='checkbox' value='".$p->cbuuid."'></td>";
        	    echo "<td>".$p->cbtext."</td>";
        	    echo "<td>".$p->cbdesc."</td>";
        	    echo "<td><a href='".base_url()."master/kategori_dokumen/kategori_dokumen/show/".$p->cbuuid."'>Lihat</a> &nbsp;
        	           <a href='".base_url()."master/kategori_dokumen/kategori_dokumen/edit/".$p->cbuuid."'>Ubah</a> &nbsp;";
        	    echo "<a href='javascript:delrecord(\"".$p->cbuuid."\")'>Hapus</a></td>";
        	   
        	    echo "</tr>";
        	    $i++;
        	    
        	   }
    	   } else {
    	       echo "<tr><td colspan='4'>Data belum ada</td></tr>";
    	   }
    	?>
    	</tbody>
    	</table>
    	<?php 
    	   if ($pageCount >1) {
    	       echo "Ke Halaman: &nbsp;";
    	       for ($i=1;$i<=$pageCount;$i++) {
    	           echo "<a href='".base_url()."administrasi/petugas/petugas/page/".$i."'>".$i."</a> &nbsp;";
    	       }
    	   }
    	?>
</div>
