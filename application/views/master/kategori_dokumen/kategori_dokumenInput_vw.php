<br>
<form class="form-horizontal" id="frmKategoriDokumen" method="POST" action="<?=base_url()."master/kategori_dokumen/kategori_dokumen/".$formAction?>">
<input type="hidden" id="cbuuid" name="cbuuid" value="<?php echo $uuid;?>">
<fieldset>
	<div class="form-group">
		<label class="col-md-2 control-label" for="cbtext">Kategori Dokumen</label>  
	  	<div class="col-md-4">
	  		<input id="cbtext" name="cbtext" placeholder="Masukkan Kategori Dokumen" class="form-control input-md" type="text" value="<?php echo (isset($dtlKatDok) ? $dtlKatDok[0]->cbtext:""); ?>">	  
	  	</div>
	</div>
	<div class="form-group">
		<label class="col-md-2 control-label" for="cbdesc">Keterangan</label>  
		<div class="col-md-4">
			<textarea id="cbdesc" name="cbdesc" placeholder="Masukkan Keterangan" class="form-control" ><?php echo (isset($dtlKatDok) ? $dtlKatDok[0]->cbdesc:""); ?></textarea>  
		</div>
	</div>
</fieldset>
</form>
