
<!-- tableview -->
<h4><i>Atur Kolom</i></h4>
<div class="table-responsive">
<form class="form-horizontal" id="frmdaftarlain" method='post' action='daftarlain/columns/<?php echo $daftar_id; ?>/deletemany'>
	<input type='hidden' id='lsgovid' name='lsgovid' value='<?php echo $govId; ?>'>
	<table class="table table-striped table-bordered table-hover" id="table-column" >
	   	<thead>
	   		<tr>
	   		<td colspan="6"><a href='<?php echo base_url(); ?>administrasi/daftarlain/columns/<?php echo $daftar_id; ?>/add'>Tambah Kolom</a></td>
	   		</tr>
    		<tr>
    			<td style="width:50px"></td>
    			<td>Nama Kolom</td>    			
    			<td>&nbsp;</td>
    			
    		</tr>
    	</thead>
    	<tbody>
    	<?php 
    	if (count($lstcolumns)>0) {
    	       $iter = 1;
    	       foreach ($lstcolumns as $c) {
        	    echo "<tr>";
        	    echo "<td><input id='id".$iter."' name='id[]' class='form-control' type='checkbox' value='".$c->lcuuid."'></td>";
        	    echo "<td>".$c->lccolnm."</td>";        	    
        	    echo "<td><a href='".base_url()."administrasi/daftarlain/columns/".$daftar_id."/edit/".$c->lcuuid."'>Ubah</a> &nbsp;";
        	    echo "<a href='javascript:delrecord(\"".$c->lcuuid."\")'>Hapus</a></td>";
        	    echo "</tr>";
        	    $iter++;
        	   }
    	   } else {
    	       echo "<tr><td colspan='6'>Kolom belum ada</td></tr>";
    	   }
    	?>
    		
    	</tbody>
    	</table>
  </form>
    	
  </div>
