
<!-- tableview -->
<h4><i>Data Daftar - <?php echo $lstdaftar[0]->lslname; ?></i></h4>
<div class="table-responsive">
<form class="form-horizontal" id="frmdata" method='post' action='<?php echo base_url(); ?>administrasi/daftarlain/data/<?php echo $daftar_id; ?>/deletemany'>
	<input type='hidden' id='ldgovid' name='ldgovid' value='<?php echo $govId; ?>'>
	<table class="table table-striped table-bordered table-hover" id="table-data" >
	   	<thead>
	   		<tr>
	   		<td colspan="6"><a href='<?php echo base_url(); ?>administrasi/daftarlain/data/<?php echo $daftar_id; ?>/add'>Tambah Data</a></td>
	   		</tr>
    		
    		<tr>
    			<td style="width:50px"></td>
    		<?php         	
    		$columns = array();
        	foreach ($lstcolumns as $c) {
                echo "<td><b>".$c->lccolnm."</b></td>";
                $columns[] = $c->lccolid;
            }
            ?>
            <td style="width:50px"></td>
        	</tr>
    	</thead>
    	<tbody>
    	<?php 
    	   $records = array();    
    	   foreach ($lstdata as $data) {
    	       $dtid = $data->lddtid;
    	       $records[$dtid][$data->ldcolid]= $data->ldtext;
    	       
    	   }
    	   
    	   $iter=0;
    	   foreach ($records as $key=>$rs) {
    	       echo "<tr><td><input type='checkbox' id='chkdata' name='id[]' value='".$key."' /></td>";
    	       foreach ($columns as $c) {
    	           $dt = "";
    	           if (array_key_exists($c,$rs)) {
    	               $dt = $rs[$c];    	               
    	           }
    	           if (strlen($dt) > 15) {
    	               $dt = substr($dt,0,15)."..";
    	           }
    	           echo "<td>".$dt."</td>";
    	       }
    	       echo "<td><a href='".base_url()."administrasi/daftarlain/data/".$daftar_id."/edit/".$key."'>Ubah</a> &nbsp;
                <a href='javascript:delrecord(\"".base_url()."administrasi/daftarlain/data/".$daftar_id."/delete/".$key."\")'>Hapus</a></td>";
    	       echo "</tr>";
    	   }
    	?>
    		
    	</tbody>
    	</table>
  </form>
    	
  </div>
