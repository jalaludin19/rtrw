
<!-- tableview -->
<h4><i>Daftar Lain-Lain</i></h4>
<div class="table-responsive">
<form class="form-horizontal" id="frmdaftarlain" method='post' action='daftarlain/deletemany'>
	<input type='hidden' id='lsgovid' name='lsgovid' value='<?php echo $govId; ?>'>
	<table class="table table-striped table-bordered table-hover" id="table-petugas" >
	   	<thead>
	   		<tr>
	   		<td colspan="6"><a href='<?php echo base_url(); ?>administrasi/daftarlain/daftarlain/add'>Tambah Daftar</a></td>
	   		</tr>
    		<tr>
    			<td style="width:50px"></td>
    			<td>Nama Daftar</td>
    			<td>Keterangan</td>
    			<td>&nbsp;</td>
    			<td>Kolom</td>
    			<td>Data Daftar</td>
    		</tr>
    	</thead>
    	<tbody>
    	<?php 
    	   if (count($lstdaftar)>0) {
    	       $iter = 1;
    	       foreach ($lstdaftar as $d) {
        	    echo "<tr>";
        	    echo "<td><input id='id".$iter."' name='id[]' class='form-control' type='checkbox' value='".$d->lsuuid."'></td>";
        	    echo "<td>".$d->lslname."</td>";
        	    echo "<td>".$d->lsdescr."</td>";
        	    echo "<td><a href='".base_url()."administrasi/daftarlain/daftarlain/edit/".$d->lsuuid."'>Ubah</a> &nbsp;";
        	    echo "<a href='javascript:delrecord(\"".$d->lslstid."\")'>Hapus</a></td>";
        	    echo "<td><a href='".base_url()."administrasi/daftarlain/columns/".$d->lslstid."'>Atur</a></td>";
        	    echo "<td><a href='".base_url()."administrasi/daftarlain/data/".$d->lslstid."'>Lihat Data</a></td>";
        	    echo "</tr>";
        	    $iter++;
        	   }
    	   } else {
    	       echo "<tr><td colspan='6'>Data belum ada</td></tr>";
    	   }
    	?>
    		
    	</tbody>
    	</table>
  </form>
    	<?php 
    	   if ($pageCount >1) {
    	       echo "Ke Halaman: &nbsp;";
    	       for ($i=1;$i<=$pageCount;$i++) {
    	           echo "<a href='".base_url()."administrasi/daftarlain/daftarlain/page/".$i."'>".$i."</a> &nbsp;";
    	       }
    	   }
    	?>
  </div>
