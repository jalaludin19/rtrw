<!-- tableview -->
<br><br>
<div class="table-responsive">
	<table class="table table-striped table-bordered table-hover" id="table-dokumen">
    	<thead>
    		<tr>
    			<td colspan="6"><?=$btnlinkadd;?></td>
    		</tr>
    		<tr>
    			<td>&nbsp;</td>
    			<td>Judul</td>
    			<td>Nama File</td>
    			<td>Keterangan</td>
    			<td>Kategori</td>
    			<td>&nbsp;</td>
    		</tr>
    	</thead>
    	<tbody>
		<?php 
    	   if (count($lstDok)>0) {
    	       foreach ($lstDok as $p) {
        	    echo "<tr>";
        	    echo "<td><input id='gdrowid' name='gdrowid' class='form-control' type='checkbox' value='".$p->gdrowid."'></td>";
        	    echo "<td>".$p->gdtitl."</td>";
        	    echo "<td>".$p->gdflnm."</td>";
        	    echo "<td>".$p->gdnote."</td>";
        	    echo "<td>".$p->gdctgr."</td>";
        	    echo "<td><a href='".base_url()."administrasi/dokumen/dokumen/show/".$p->gdrowid."'>Show</a> &nbsp;
        	           <a href='".base_url()."administrasi/dokumen/dokumen/edit/".$p->gdrowid."'>Edit</a> &nbsp;";
        	    echo "<a href='javascript:delrecord(\"".$p->gdrowid."\")'>Delete</a></td>";
        	   
        	    echo "</tr>";
        	    
        	   }
    	   } else {
    	       echo "<tr><td colspan='6'>Data belum ada</td></tr>";
    	   }
    	?>
    	</tbody>
    	</table>
    	<?php 
    	   if ($pageCount >1) {
    	       echo "Ke Halaman: &nbsp;";
    	       for ($i=1;$i<=$pageCount;$i++) {
    	           echo "<a href='".base_url()."administrasi/petugas/petugas/page/".$i."'>".$i."</a> &nbsp;";
    	       }
    	   }
    	?>
</div>
<!-- <br>
<form class="form-horizontal">
<fieldset>
	<div class="form-group">
		<label class="col-md-2 control-label" for="gdtitl">Judul</label>  
	  	<div class="col-md-4">
	  		<input id="gdtitl" name="gdtitl" placeholder="masukkan judul" class="form-control input-md" type="text">	  
	  	</div>
	</div>
	<div class="form-group">
		<label class="col-md-2 control-label" for="gdflnm">Dokumen</label>  
		<div class="col-md-4">
			<input id="gdflnm" name="gdflnm" type="file">
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-md-2 control-label" for="gdcatid">Keterangan</label>  
		<div class="col-md-4">
			<textarea id="gdcatid" name="gdcatid" placeholder="masukkan keterangan" class="form-control" ></textarea>  
		</div>
	</div>
	<div class="form-group">
	  <label class="col-md-2 control-label" for="gdnote">Kategori</label>  
	  <div class="col-md-4">
		  <select id="gdnote" name="gdnote" placeholder="pilih kategori" class="form-control">
			  <option>1</option>
			  <option>2</option>
			  <option>3</option>
			  <option>4</option>
			  <option>5</option>
		  </select>
	 
	  </div>
	  <?=$btnlinkadd;?>
	</div>
</fieldset>
</form>-->

<!-- tableview
<h4>Daftar Dokumen</h4>
<div class="table-responsive">
	<table class="table table-striped table-bordered table-hover" id="table-dokumen">
    	<thead>
    		<tr>
    			<td></td>
    			<td>judul</td>
    			<td>dokumen</td>
    			<td>keterangan</td>
    			<td>kategori</td>
    		</tr>
    	</thead>
    	<tbody>
    		<tr>
    			<td><input id="" name="" class="form-control" type="checkbox"></td>
    			<td>test</td>
    			<td><a href="javascript:void(0)">test.jpg</a></td>
    			<td>ini test</td>
    			<td>test</td>
    		</tr>
    	</tbody>
</table>
</div>-->

