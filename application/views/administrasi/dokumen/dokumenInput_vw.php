<br>
<form class="form-horizontal" id="frmDokumen" method="POST" action="<?=base_url()."administrasi/dokumen/dokumen/".$formAction?>" enctype="multipart/form-data">
<input type="hidden" id="gdrowid" name="gdrowid" value="<?php echo $rowid;?>">
<fieldset>
	<div class="form-group">
		<label class="col-md-2 control-label" for="gdtitl">Judul</label>  
	  	<div class="col-md-4">
	  		<input id="gdtitl" name="gdtitl" placeholder="masukkan judul" class="form-control input-md" type="text">	  
	  	</div>
	</div>
	<div class="form-group">
		<label class="col-md-2 control-label" for="gdflnm">Dokumen</label>  
		<div class="col-md-4">
			<input id="gdflnm" name="gdflnm" type="file">
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-md-2 control-label" for="gdcatid">Keterangan</label>  
		<div class="col-md-4">
			<textarea id="gdcatid" name="gdcatid" placeholder="masukkan keterangan" class="form-control" ></textarea>  
		</div>
	</div>
	<div class="form-group">
	  <label class="col-md-2 control-label" for="gdnote">Kategori</label>  
	  <div class="col-md-4">
		  <select id="gdnote" name="gdnote" placeholder="pilih kategori" class="form-control">
			  <?php 
			  foreach ($lstKatDok as $p) {
			      echo '<option value="'.$p->cbuuid.'">'.$p->cbtext.'</option>';
			  }
			  ?>
		  </select>	 
	  </div>
	</div>
</fieldset>
</form>