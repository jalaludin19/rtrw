<br>

<form class="form-horizontal" id="frmpetugas" method="post" action="<?php echo base_url()."administrasi/petugas/petugas/".$formAction; ?>">
<input type="hidden" id="ofgovid" name="ofgovid" value="<?php echo $govId;?>" >
<input type="hidden" id="id" name="id" value="<?php echo $id;?>">
<fieldset>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-2 control-label" for="ofprnam">Nama Petugas</label>  
  <div class="col-md-4">
	  <input id="ofprnam" name="ofprnam" class="form-control input-md" type="text" value="<?php echo (isset($dtlpetugas) ? $dtlpetugas[0]->ofprnam:""); ?>">
 
  </div>
</div>

<div class="form-group">
  <label class="col-md-2 control-label" for="ofposid">Jabatan</label>  
  <div class="col-md-4">
	  <select id="ofposid" name="ofposid" class="form-control">
			<?php 
			if (count($lstjabatan)>0) {
    	      
    	       foreach ($lstjabatan as $j) {
    	           echo "<option value='".$j->cbstky."'>".$j->cbtext."</option>";
        	   
        	   }
    	   } 
    	?>
	  </select>
 
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-2 control-label" for="ofprsid">NIK</label>  
  <div class="col-md-4">
  <input id="ofprnik" name="ofprnik" class="form-control input-md" type="text" value="<?php echo (isset($dtlpetugas) ? $dtlpetugas[0]->ofprnik:""); ?>">
  
  </div>
</div>

<div class="form-group">
  <label class="col-md-2 control-label" for="ofaddrs">Alamat</label>  
  <div class="col-md-4">
  
  <textarea id="ofaddrs" name="ofaddrs" class="form-control" >
  <?php echo (isset($dtlpetugas) ? $dtlpetugas[0]->ofaddrs:""); ?>
  </textarea>
  </div>
</div>

<div class="form-group">
  <label class="col-md-2 control-label" for="ofcity">Kota</label>  
  <div class="col-md-4">
  <input id="ofcity" name="ofcity"  class="form-control input-md" type="text" value="<?php echo (isset($dtlpetugas) ? $dtlpetugas[0]->ofcity:""); ?>">
  
  </div>
</div>

<div class="form-group">
  <label class="col-md-2 control-label" for="ofdatfr">Tanggal Mulai</label>  
  <div class="col-md-4">
	 <div class='input-group date' id='ofdatfr'>
                    <input type='text' class="form-control" name='ofdatfr' value="<?php echo (isset($dtlpetugas) ? $dtlpetugas[0]->ofdatfr:""); ?>"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>

  
  </div>
</div>

<div class="form-group">
  <div class="col-md-2"> &nbsp;
  </div>
  <div class="col-md-6">
  <input id="ofactiv" name="ofactiv"  type="checkbox" class="form-check-input" value='1' <?php echo (isset($dtlpetugas) ? ($dtlpetugas[0]->ofactiv==1 ? "checked" : "") : "checked"); ?>> Petugas Aktif
  
  </div>
</div>



<div class="form-group">
  <label class="col-md-2 control-label" for="ofdatto">Tanggal Akhir</label>  
  <div class="col-md-4">
	 <div class='input-group date' id='ofdatto'>
                    <input type='text' class="form-control" name='ofdatto' value="<?php echo (isset($dtlpetugas) ? $dtlpetugas[0]->ofdatto:""); ?>"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>

  
  </div>
</div>

</fieldset>
<br>
</form>
<script type="text/javascript">
            $(function () {
                $('#ofdatfr').datetimepicker({
                	format: 'YYYY-MM-DD'
                });

                $('#ofdatto').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
            });
        </script>