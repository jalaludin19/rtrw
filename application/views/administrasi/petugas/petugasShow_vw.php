<br>

<fieldset>


<div class="form-group">
<label class="col-md-2 control-label">Nama Petugas</label>
<div class="col-md-4">
<?php echo $dtlpetugas[0]->ofprnam; ?>

</div>
</div>
<br>
<div class="form-group">
<label class="col-md-2 control-label" for="ofposid">Jabatan</label>
<div class="col-md-4">
<?php echo $dtlpetugas[0]->ofposid; ?>
</div>
</div>
<br>
<div class="form-group">
<label class="col-md-2 control-label" for="ofprsid">NIK</label>
<div class="col-md-4">
<?php echo $dtlpetugas[0]->ofprnik; ?>
</div>
</div>
<br>
<div class="form-group">
<label class="col-md-2 control-label" for="ofaddrs">Alamat</label>
<div class="col-md-4">
<?php echo $dtlpetugas[0]->ofaddrs; ?>
  </div>
</div>
<br>
<div class="form-group">
  <label class="col-md-2 control-label" for="ofcity">Kota</label>  
  <div class="col-md-4">
 <?php echo $dtlpetugas[0]->ofcity; ?>
  
  </div>
</div>
<br>
<div class="form-group">
  <label class="col-md-2 control-label" >Tanggal Mulai</label>  
  <div class="col-md-4">
	<?php echo $dtlpetugas[0]->ofdatfr; ?>
  
  </div>
</div>
<br>
<div class="form-group">
  <label class="col-md-2 control-label"> Aktif
  </label>
  <div class="col-md-4">
<?php echo ($dtlpetugas[0]->ofactiv==1 ? "Ya" : "Tidak"); ?>
  
  </div>
</div>
<br>


<div class="form-group">
  <label class="col-md-2 control-label" >Tanggal Akhir</label>  
  <div class="col-md-4">
	 <?php echo $dtlpetugas[0]->ofdatto; ?>
  </div>
</div>

</fieldset>
<br>