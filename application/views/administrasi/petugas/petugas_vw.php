
<!-- tableview -->
<h4><i>Daftar Petugas</i></h4>
<div class="table-responsive">
<form class="form-horizontal" id="frmpetugas" method='post' action='petugas/deletemany'>
	<input type='hidden' id='ofgovid' name='ofgovid' value='<?php echo $govId; ?>'>
	<table class="table table-striped table-bordered table-hover" id="table-petugas" >
	   	<thead>
	   		<tr>
	   		<td colspan="6"><a href='<?php echo base_url(); ?>administrasi/petugas/petugas/add'>Tambah Petugas</a></td>
	   		</tr>
    		<tr>
    			<td style="width:50px"></td>
    			<td>Nama</td>
    			<td>Jabatan</td>
    			<td>Tanggal Mulai</td>
    			<td>Petugas Aktif</td>
    			<td>&nbsp;</td>
    		</tr>
    	</thead>
    	<tbody>
    	<?php 
    	   if (count($lstpetugas)>0) {
    	       $iter = 1;
        	   foreach ($lstpetugas as $p) {
        	    echo "<tr>";
        	    echo "<td><input id='id".$iter."' name='id[]' class='form-control' type='checkbox' value='".$p->ofuuid."'></td>";
        	    echo "<td>".$p->ofprnam."</td>";
        	    echo "<td>".$p->ofposid."</td>";
        	    echo "<td>".$p->ofdatfr."</td>";
        	    echo "<td>".($p->ofactiv==1 ? "Ya":"Tidak")."</td>";
        	    echo "<td><a href='".base_url()."administrasi/petugas/petugas/show/".$p->ofuuid."'>Lihat</a> 
                &nbsp;<a href='".base_url()."administrasi/petugas/petugas/edit/".$p->ofuuid."'>Ubah</a> &nbsp;";
        	    echo "<a href='javascript:delrecord(\"".$p->ofuuid."\")'>Hapus</a></td>";
        	   
        	    echo "</tr>";
        	    $iter++;
        	   }
    	   } else {
    	       echo "<tr><td colspan='5'>Data belum ada</td></tr>";
    	   }
    	?>
    		
    	</tbody>
    	</table>
  </form>
    	<?php 
    	   if ($pageCount >1) {
    	       echo "Ke Halaman: &nbsp;";
    	       for ($i=1;$i<=$pageCount;$i++) {
    	           echo "<a href='".base_url()."administrasi/petugas/petugas/page/".$i."'>".$i."</a> &nbsp;";
    	       }
    	   }
    	?>
  </div>
