<!-- tableview -->
<h4><i>Daftar Petugas</i></h4>
<div class="table-responsive">

<table class="table table-striped table-bordered table-hover"  >
<thead>
<tr>
	<td colspan="6"><a href='<?php echo base_url()."administrasi/pengumuman/pengumuman/add"; ?>'>Tambah Pengumuman</a></td>
</tr>
<tr>
	<td style="width:50px"></td>
	<td>Judul Pengumuman</td>
	<td>Isi Pengumuman</td>
	<td>Tanggal Mulai</td>
	<td>Tanggal Berakhir</td>
	<td>&nbsp;</td>
</tr>
</thead>
<tbody>
<?php
if (count($lstpengumuman)>0) {
    foreach ($lstpengumuman as $p) {
        echo "<tr>";
            echo "<td align='center'><input id='pmgovid' name='pmgovid' class='form-control' type='checkbox' value='".$p->pmgovid."'></td>";
            echo "<td>".$p->pmasbjc."</td>";
            echo "<td>".$p->pmadesc."</td>";
            echo "<td>".$p->pmastdt."</td>";
            echo "<td>".$p->pmaendt."</td>";
            echo "<td><a href='".base_url()."administrasi/pengumuman/pengumuman/show/".$p->pmuuid."'>Lihat</a>
                &nbsp;<a href='".base_url()."administrasi/pengumuman/pengumuman/edit/".$p->pmuuid."'>Ubah</a> &nbsp;";
            echo "<a href='javascript:delrecord(\"".$p->pmuuid."\")'>Hapus</a></td>";
                   
        echo "</tr>";
        
    }
} else {
    echo "<tr><td colspan='5'>Data belum ada</td></tr>";
}
?>
    		
</tbody>
</table>
</div>

