<br>
<form class="form-horizontal" id="frmpengumuman" method="post" action="<?php echo base_url()."administrasi/pengumuman/pengumuman/".$formAction; ?>" >
<input type="hidden" id="ofgovid" name="ofgovid" value="<?php echo $govId;?>" >
<input type="hidden" id="pmuuid" name="pmuuid" value="<?php echo $pmuuid;?>">
<fieldset>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-2 control-label" for="textinput">Tanggal Mulai</label>  
  <div class="col-md-4">
	 <div class='input-group date'>
        <input name='pmastdt' id='pmastdt' type='text' value="<?php echo (isset($getpengumuman)? $getpengumuman[0]->pmastdt:""); ?>" class="form-control" />
        <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
        </span>
    </div>  
  </div>
</div>
<div class="form-group">
  <label class="col-md-2 control-label" for="textinput">Tanggal Berakhir</label>  
  <div class="col-md-4">
	 <div class='input-group date' >
        <input name='pmaendt' id='pmaendt' type='text' value="<?php echo (isset($getpengumuman)? $getpengumuman[0]->pmaendt:""); ?>" class="form-control" />
        <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
        </span>
    </div>  
  </div>
</div>

<div class="form-group">
  <label class="col-md-2 control-label" for="cnemail">Subject Pengumuman</label>  
  <div class="col-md-4">
  	<input id="pmasbjc" name="pmasbjc" placeholder="Masukan subject pengumuman" value="<?php echo (isset($getpengumuman)? $getpengumuman[0]->pmasbjc:""); ?>" class="form-control input-md">  
  </div>
</div>

<div class="form-group">
  <label class="col-md-2 control-label" for="pmadesc">Isi Pengumuman</label>  
  <div class="col-md-4">
  	<textarea  id="pmadesc" name="pmadesc" placeholder="Masukan Pengumuman" class="form-control input-md"><?php  echo htmlspecialchars($getpengumuman[0]->pmadesc); ?> </textarea>
  </div>
</div>

<div class="form-group">
  <label class="col-md-2 control-label" for="cnstats">Status</label>  
  <div class="col-md-4">
  	<input id="cnstats" name="cnstats" class="form-control input-md" type="text" value=""<?php echo (isset($getpengumuman)? $getpengumuman[0]->pmastat:""); ?>" class="form-control input-md"> ">  
  </div>
</div>

</fieldset>
</form>
<script type="text/javascript">
    $(function () {
        $('#pmastdt').datetimepicker({
        	format: 'YYYY-MM-DD'
        });
        $('#pmaendt').datetimepicker({
        	format: 'YYYY-MM-DD'
        });
    });
</script>