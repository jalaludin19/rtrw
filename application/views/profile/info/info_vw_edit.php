<br>
<form class="form-horizontal" id='frminfo' name='frminfo' method='POST' action='<?php echo $frmaction; ?>' enctype='multipart/form-data'>
<?php  

foreach ($data_info as $value) { ?>
<!-- Text input-->
<div class="form-group">
	
  <label class="col-md-3 control-label" for="provinsi">Provinsi</label>  
  <div class="col-md-4">
	  <select id="cbprov" name="cbprov" class="form-control" onchange='getKab(this.value)'>
	  			  <option>-- Select Provinsi --</option>
				 <?php foreach ($listprov as $row) { 
					if (!empty($cbprov)){
						$thisval = $cbprov;
					}
					else{
						$thisval = $value->GPPRVID;
					}
				 ?>
				 	<?php if ($thisval==$row->pvid){ ?>
				 	 	<option value=<?php echo $row->pvid; ?> selected><?php echo $row->pvname; ?></option>
				 	<?php  }else { ?>
				 	
				 	
				 		<option value=<?php echo $row->pvid; ?>><?php echo $row->pvname; ?></option>
				 	<?php } ?>
				 	
				 	
				 <?php } ?>
	  </select>
 
  </div>
</div>

<div class="form-group">
  <label class="col-md-3 control-label" for="provinsi">Kab/Kota Madya</label>  
  <div class="col-md-4">
  	  
	  <select id="cbkab" name="cbkab" class="form-control" onchange='getKec(this.value)'>
	   	 <option>-- Select Kabupaten --</option>
	  	 <?php 
	  	 
	  	 foreach ($listkab as $row) { ?>
	  	 
	  	 	<?php 
	  	 	if (!empty($cbkab)){
				$thisval = $cbkab;
			}
			else{
				$thisval = $value->GPRGCID;
			}
	  	 	if ($thisval==$row->rgid){ 
			?>
				 	 	<option value=<?php echo $row->rgid; ?> selected><?php echo $row->rgname; ?></option>
			<?php  }else { ?>
				 	
				 	
				 	<option value=<?php echo $row->rgid; ?>><?php echo $row->rgname; ?></option>
			<?php } ?>
				 	
				 	
		<?php } ?>
	  	
	  </select>
 
  </div>
</div>
<div class="form-group">
  <label class="col-md-3 control-label" for="provinsi">Kecamatan</label>  
  <div class="col-md-4">
 		
	  <select id="cbkec" name="cbkec" class="form-control" onchange='getKel(this.value)'>
	 
	  <option>-- Select Kecamatan --</option>
		<?php 
		 
	  	 foreach ($listkec as $row) { 
			
			if (!empty($cbkec)){
				$thisval = $cbkec;
			}
			else{
				$thisval = $value->GPDISID;
			}
		 
		 ?>
				
	  	 		<?php if ($thisval==$row->dsid){ ?>
				 	 	<option value=<?php echo $row->dsid; ?> selected><?php echo $row->dsname; ?></option>
				<?php  }else { ?>
				 	
				 	
				 	<option value=<?php echo $row->dsid; ?>><?php echo $row->dsname; ?></option>
				<?php } ?>
				 	
				
				 	
		<?php } ?>
	  </select>
 
  </div>
</div>
<div class="form-group">
  <label class="col-md-3 control-label" for="provinsi">Nama Desa/Kelurahan</label>  
  <div class="col-md-4">
	  <select id="cbkel" name="cbkel" class="form-control">
	    <option>-- Select Kelurahan --</option>
		<?php 
	  	
	  	 foreach ($listkel as $row) { ?>
	  	 
	  	 <?php if ($value->GPVILID==$row->vlid){ ?>
				 	 	<option value=<?php echo $row->vlid; ?> selected><?php echo $row->vlname; ?></option>
				<?php  }else { ?>
				 	
				 	
				 	<option value=<?php echo $row->vlid; ?>><?php echo $row->vlname; ?></option>
				<?php } ?>
				
				 	
		<?php } ?>
	  </select>
 
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-3 control-label" for="textinput">RT/RW</label>  
  <div class="col-md-2">
  	<input id="textinput" name="gprtnum" id="gprtnum" placeholder="RT" class="form-control input-md" type="text" value="<?php echo $value->GPRTNUM; ?>"></input>
  
  </div>
  <div class="col-md-2">
  <input name="gprwnum" id="gprwnum" placeholder="RW" class="form-control input-md" type="text" value="<?php echo $value->GPRWNUM; ?>"></input>
  
  </div>
</div>
<div class="form-group">
  <label class="col-md-3 control-label" for="textinput">Alamat</label>  
  <div class="col-md-4">
  <textarea class="form-control" cols="5" id='gpaddrs' name='gpaddrs' ><?php echo $value->GPADDRS; ?></textarea>
  
  </div>
</div>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-3 control-label" for="textinput">Kode pos</label>  
  <div class="col-md-3">
  <input id="gpposcd" name="gpposcd" placeholder="Kode pos" class="form-control input-md" type="text" value="<?php echo $value->GPPOSCD; ?>">
  
  </div>
</div>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-3 control-label" for="textinput">No Telepon</label>  
  <div class="col-md-4">
  <input id="gpphone" name="gpphone" placeholder="No Telepon" class="form-control input-md" type="text" value="<?php echo $value->GPPHONE; ?>">
  
  </div>
</div>
<div class="form-group">
  <label class="col-md-3 control-label" for="textinput">Email</label>  
  <div class="col-md-4">
  <input id="gpemail" name="gpemail" placeholder="Email" class="form-control input-md" type="email" value="<?php echo $value->GPEMAIL; ?>">
  
  </div>
</div>
<div class="form-group">
  <label class="col-md-3 control-label" for="textinput">Nama ketua RT</label>  
  <div class="col-md-4">
  <input id="gprtnm" name="gprtnm" placeholder="Nama ketua RT" class="form-control input-md" type="text" value="<?php echo $value->GPRTNM; ?>">
  
  </div>
</div>
<div class="form-group">
  <label class="col-md-3 control-label" for="textinput">Nama ketua RW</label>  
  <div class="col-md-4">
  <input id="gprwnm" name="gprwnm" placeholder="Nama ketua RW" class="form-control input-md" type="text" value="<?php echo $value->GPRWNM; ?>">
  
  </div>
</div>
<div class="form-group">
  <label class="col-md-3 control-label" for="textinput">Logo</label>  
  <div class="col-md-4">
  <input type="file" id="gplogo" name="gplogo" accept="image/*">
    <p class="help-block">Logo for RT/RW</p>
  
  </div>
</div>
<input type="hidden" id="mode" name="mode" value="" ></input>

<?php  } ?>
</form>

<script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker({
                	format: 'DD/MM/YYYY'
                });
            });
        </script>
