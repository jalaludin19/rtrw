<br>
<div class="row">
<div class="col-md-12">
<H1>Profile</H1>
<?php foreach ($data_info as $value) { ?>

<div class="col-sm-6 col-md-3">
    <a href="#" class="thumbnail">
      
      <img src="<?php echo base_url(); ?>/assets/images/IMG_20170618_124753_HDR.jpg" alt="Profile picture">
    </a>
</div>

<table class="table table-striped">
 	<thead>
 		
 	</thead>
 	<tbody>
 		
 		<tr>
 			<td>Nama Propinsi</td>
 			<td>:</td>
 			<td><?php echo $value->propinsi ?></td>
 		</tr>
 		<tr>
 			<td>Nama Kabupaten</td>
 			<td>:</td>
 			<td><?php echo $value->kabupaten ?></td>
 		</tr>
 		<tr>
 			<td>Nama Kecamatan</td>
 			<td>:</td>
 			<td><?php echo $value->kecamatan ?></td>
 		</tr>
 		<tr>
 			<td>Nama Desa/kelurahan</td>
 			<td>:</td>
 			<td><?php echo $value->kelurahan ?></td>
 		</tr>
 		
 		<tr>
 			<td>Rw/Rt</td>
 			<td>:</td>
 			<td><?php echo $value->GPRTNUM."/".$value->GPRWNUM; ?></td>
 		</tr>
 		<tr>
 			<td>Kode pos</td>
 			<td>:</td>
 			<td><?php echo $value->GPPOSCD; ?></td>
 		</tr>
		<tr>
 			<td>Alamat</td>
 			<td>:</td>
 			<td><?php echo $value->GPADDRS; ?></td>
 		</tr>
		<tr>
 			<td>No Telepon</td>
 			<td>:</td>
 			<td><?php echo $value->GPPHONE; ?></td>
 		</tr>
		<tr>
 			<td>Email</td>
 			<td>:</td>
 			<td><?php echo $value->GPEMAIL; ?></td>
 		</tr>
		<tr>
 			<td>Nama ketua RT</td>
 			<td>:</td>
 			<td><?php echo $value->GPRTNM; ?></td>
 		</tr>
		<tr>
 			<td>Nama ketua RW</td>
 			<td>:</td>
 			<td><?php echo $value->GPRWNM; ?></td>
 		</tr>
 		<?php } ?>
 	</tbody>
</table>
</div>
</div>
