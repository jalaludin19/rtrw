
<!-- tableview -->
<h4><i>Daftar Pengurus</i></h4>
<div class="table-responsive">
<form class="form-horizontal" id="frmpengurus" method='post' action='pengurus/deletemany'>
	<input type='hidden' id='ofgovid' name='ofgovid' value='<?php echo $govId; ?>'>
	<table class="table table-striped table-bordered table-hover" id="table-pengurus" >
	   	<thead>
	   		<tr>
	   		<td colspan="6"><a href='<?php echo base_url(); ?>profile/pengurus/pengurus/add'>Tambah Pengurus</a></td>
	   		</tr>
    		<tr>
    			<td style="width:50px"></td>
    			<td>Nama</td>
    			<td>Jabatan</td>
    			<td>Tanggal Mulai</td>
    			<td>Pengurus Aktif</td>
    			<td>&nbsp;</td>
    		</tr>
    	</thead>
    	<tbody>
    	<?php 
    	   if (count($lstpengurus)>0) {
    	       $iter = 1;
        	   foreach ($lstpengurus as $p) {
        	    echo "<tr>";
        	    echo "<td><input id='id".$iter."' name='id[]' class='form-control' type='checkbox' value='".$p->mguuid."'></td>";
        	    echo "<td>".$p->mgprnam."</td>";
        	    echo "<td>".$p->mgposid."</td>";
        	    echo "<td>".$p->mgdatfr."</td>";
        	    echo "<td>".($p->mgactiv==1 ? "Ya":"Tidak")."</td>";
        	    echo "<td><a href='".base_url()."profile/pengurus/pengurus/show/".$p->mguuid."'>Lihat</a> 
                &nbsp;<a href='".base_url()."profile/pengurus/pengurus/edit/".$p->mguuid."'>Ubah</a> &nbsp;";
        	    echo "<a href='javascript:delrecord(\"".$p->mguuid."\")'>Hapus</a></td>";
        	   
        	    echo "</tr>";
        	    $iter++;
        	   }
    	   } else {
    	       echo "<tr><td colspan='5'>Data belum ada</td></tr>";
    	   }
    	?>
    		
    	</tbody>
    	</table>
  </form>
    	<?php 
    	   if ($pageCount >1) {
    	       echo "Ke Halaman: &nbsp;";
    	       for ($i=1;$i<=$pageCount;$i++) {
    	           echo "<a href='".base_url()."profile/pengurus/pengurus/page/".$i."'>".$i."</a> &nbsp;";
    	       }
    	   }
    	?>
  </div>
