<br>

<form class="form-horizontal" id="frmpengurus" method="post" action="<?php echo base_url()."profile/pengurus/pengurus/".$formAction; ?>">
<input type="hidden" id="mggovid" name="mggovid" value="<?php echo $govId;?>" >
<input type="hidden" id="id" name="id" value="<?php echo $id;?>">
<fieldset>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-2 control-label" for="mgprnam">Nama Pengurus</label>  
  <div class="col-md-4">
	  <input id="mgprnam" name="mgprnam" class="form-control input-md" type="text" value="<?php echo (isset($dtlpengurus) ? $dtlpengurus[0]->mgprnam:""); ?>">
 
  </div>
</div>

<div class="form-group">
  <label class="col-md-2 control-label" for="mgposid">Jabatan</label>  
  <div class="col-md-4">
	  <select id="mgposid" name="mgposid" class="form-control">
			<?php 
			if (count($lstjabatan)>0) {
    	      
    	       foreach ($lstjabatan as $j) {
    	           echo "<option value='".$j->cbstky."'>".$j->cbtext."</option>";
        	   
        	   }
    	   } 
    	?>
	  </select>
 
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-2 control-label" for="mgprsid">NIK</label>  
  <div class="col-md-4">
  <input id="mgprnik" name="mgprnik" class="form-control input-md" type="text" value="<?php echo (isset($dtlpengurus) ? $dtlpengurus[0]->mgprnik:""); ?>">
  
  </div>
</div>

<div class="form-group">
  <label class="col-md-2 control-label" for="mgaddrs">Alamat</label>  
  <div class="col-md-4">
  
  <textarea id="mgaddrs" name="mgaddrs" class="form-control" >
  <?php echo (isset($dtlpengurus) ? $dtlpengurus[0]->mgaddrs:""); ?>
  </textarea>
  </div>
</div>

<div class="form-group">
  <label class="col-md-2 control-label" for="mgcity">Kota</label>  
  <div class="col-md-4">
  <input id="mgcity" name="mgcity"  class="form-control input-md" type="text" value="<?php echo (isset($dtlpengurus) ? $dtlpengurus[0]->mgcity:""); ?>">
  
  </div>
</div>

<div class="form-group">
  <label class="col-md-2 control-label" for="mgdatfr">Tanggal Mulai</label>  
  <div class="col-md-4">
	 <div class='input-group date' id='mgdatfr'>
                    <input type='text' class="form-control" name='mgdatfr' value="<?php echo (isset($dtlpengurus) ? $dtlpengurus[0]->mgdatfr:""); ?>"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>

  
  </div>
</div>

<div class="form-group">
  <div class="col-md-2"> &nbsp;
  </div>
  <div class="col-md-6">
  <input id="mgactiv" name="mgactiv"  type="checkbox" class="form-check-input" value='1' <?php echo (isset($dtlpengurus) ? ($dtlpengurus[0]->mgactiv==1 ? "checked" : "") : "checked"); ?>> Pengurus Aktif
  
  </div>
</div>



<div class="form-group">
  <label class="col-md-2 control-label" for="mgdatto">Tanggal Akhir</label>  
  <div class="col-md-4">
	 <div class='input-group date' id='mgdatto'>
                    <input type='text' class="form-control" name='mgdatto' value="<?php echo (isset($dtlpengurus) ? $dtlpengurus[0]->mgdatto:""); ?>"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>

  
  </div>
</div>

</fieldset>
<br>
</form>
<script type="text/javascript">
            $(function () {
                $('#mgdatfr').datetimepicker({
                	format: 'YYYY-MM-DD'
                });

                $('#mgdatto').datetimepicker({
                	format: 'YYYY-MM-DD'
                });
            });
        </script>