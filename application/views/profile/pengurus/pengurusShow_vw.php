<br>

<fieldset>


<div class="form-group">
<label class="col-md-2 control-label">Nama Pengurus</label>
<div class="col-md-4">
<?php echo $dtlpengurus[0]->mgprnam; ?>

</div>
</div>
<br>
<div class="form-group">
<label class="col-md-2 control-label" for="mgposid">Jabatan</label>
<div class="col-md-4">
<?php echo $dtlpengurus[0]->mgposid; ?>
</div>
</div>
<br>
<div class="form-group">
<label class="col-md-2 control-label" for="mgprsid">NIK</label>
<div class="col-md-4">
<?php echo $dtlpengurus[0]->mgprnik; ?>
</div>
</div>
<br>
<div class="form-group">
<label class="col-md-2 control-label" for="mgaddrs">Alamat</label>
<div class="col-md-4">
<?php echo $dtlpengurus[0]->mgaddrs; ?>
  </div>
</div>
<br>
<div class="form-group">
  <label class="col-md-2 control-label" for="mgcity">Kota</label>  
  <div class="col-md-4">
 <?php echo $dtlpengurus[0]->mgcity; ?>
  
  </div>
</div>
<br>
<div class="form-group">
  <label class="col-md-2 control-label" >Tanggal Mulai</label>  
  <div class="col-md-4">
	<?php echo $dtlpengurus[0]->mgdatfr; ?>
  
  </div>
</div>
<br>
<div class="form-group">
  <label class="col-md-2 control-label"> Aktif
  </label>
  <div class="col-md-4">
<?php echo ($dtlpengurus[0]->mgactiv==1 ? "Ya" : "Tidak"); ?>
  
  </div>
</div>
<br>


<div class="form-group">
  <label class="col-md-2 control-label" >Tanggal Akhir</label>  
  <div class="col-md-4">
	 <?php echo $dtlpengurus[0]->mgdatto; ?>
  </div>
</div>

</fieldset>
<br>