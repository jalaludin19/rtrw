<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<html>
<head>
	<!-- Bootstrap Core CSS -->
		
	<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
	<!-- MetisMenu CSS -->
    <link href="<?php echo base_url();?>assets/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
	 <!-- Custom CSS -->
    <link href="<?php echo base_url();?>assets/css/sb-admin-2.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url();?>assets/css/plugins/dataTables.bootstrap.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/css/rtw.css" rel="stylesheet">
	<script src="<?= base_url();?>assets/js/jquery-1.11.0.js"></script>
	 <!--[if lt IE 9]><script src="assets/js/shim/iehtmlshiv.js"></script><![endif]-->
	
	
	<script src="<?= base_url();?>assets/js/bootstrap.min.js"></script>
	 <!-- Metis Menu Plugin JavaScript -->
    <script src="<?= base_url();?>assets/js/plugins/metisMenu/metisMenu.min.js"></script>
    <script src="<?= base_url();?>assets/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="<?= base_url();?>assets/js/plugins/dataTables/dataTables.bootstrap.js"></script>
    <script src="<?= base_url();?>assets/js/moment.js"></script>
    <script src="<?= base_url();?>assets/js/bootstrap-datetimepicker.min.js"></script>
    
    
    <!-- Custom Theme JavaScript -->
    <script src="<?= base_url();?>assets/js/sb-admin-2.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/javascript/<?php echo $jspath;?>"></script>

	<title>RT/RW APP</title>
</head>
<body>
 <!-- Fixed navbar -->
    <div class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
         
          <a class="navbar-brand" href="#">RTRW Application</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
           
           
          </ul>
       
        </div><!--/.nav-collapse -->
        
      </div>
      
       <div class="nav-form">
        <div class="nav-title">
				<?php if (isset( $title)) echo $title; ?>
		
		</div>
		<div class="nav-button">
			<?php if (isset( $btnsave)) echo $btnsave; ?>
			<?php if (isset( $btnadd)) echo $btnadd; ?>
			<?php if (isset( $btnedit)) echo $btnedit; ?>
			<?php if (isset( $btnback)) echo $btnback; ?>
			<?php if (isset( $btndel)) echo $btndel; ?>
		</div>
		</div>
    </div>
  <br>
  <br>
  <br>
  <br>
  <br>
  
 
 
		
      
			<div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                      
                        <li>
                            <a class="active" href="<?php echo base_url(); ?>home"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li <?php if($this->uri->segment(1)=="profile"){echo 'class="active"';}?>>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Profil<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
								<li>
                                    <a href='<?php echo base_url(); ?>profile/info/info' <?php if($this->uri->segment(2)=="info"){echo 'class="active"';}?>>Info</a>
                                </li>
                                
                                <li>
                                    <a  href='<?php echo base_url(); ?>profile/pengurus/pengurus' <?php if($this->uri->segment(2)=="pengurus"){echo 'class="active"';}?>>Pengurus</a>
                                </li>
								
								
                               
								
                            </ul>
                            <!-- /.nav-second-level -->
                        
                         <li <?php if($this->uri->segment(1)=="administrasi"){echo 'class="active"';}?>>
                            <a href="#"><i class="fa fa-files-o fa-fw"></i>Administrasi<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                            	 <li>
                                    <a href="<?php echo base_url(); ?>administrasi/pengumuman/pengumuman" <?php if($this->uri->segment(2)=="pengumuman"){echo 'class="active"';}?>>Pengumuman</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>administrasi/dokumen/dokumen" <?php if($this->uri->segment(2)=="dokumen"){echo 'class="active"';}?>>Dokumen</a>
                                </li>
								<li>
                                    <a href="<?php echo base_url(); ?>administrasi/petugas/petugas" <?php if($this->uri->segment(2)=="petugas"){echo 'class="active"';}?>>Petugas</a>
                                </li>
								<li>
                                    <a href="<?php echo base_url(); ?>administrasi/daftarlain/daftarlain" <?php if($this->uri->segment(2)=="daftarlain"){echo 'class="active"';}?>>Daftar Lain-lain</a>
                                </li>
                              
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li <?php if($this->uri->segment(1)=="master"){echo 'class="active"';}?>>
                        	<a href="#"><i class="fa fa-files-o fa-fw"></i>Master<span class="fa arrow"></span></a>
                        	<ul class="nav nav-second-level">
                        	<li>
                                  <a href="<?php echo base_url(); ?>master/jabatan/jabatan" <?php if($this->uri->segment(2)=="jabatan"){echo 'class="active"';}?>>Jabatan</a>
                            </li>
                            <li>
                                  <a href="<?php echo base_url(); ?>master/kategori_dokumen/kategori_dokumen" <?php if($this->uri->segment(2)=="kategori_dokumen"){echo 'class="active"';}?>>Kategori Dokumen</a>
                            </li>
                            </ul>
                        </li>
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
			
       
		 <!-- End Navigation  -->
  		 <div id="page-wrapper">
    	
   