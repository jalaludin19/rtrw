<?php
function btnsave($url) {
    return '<button type="button" class="btn btn-success btn-sm" onclick="save(\''.$url.'\')">Simpan</button>';
}

function btnedit($url) {
    return '<button type="button" class="btn btn-primary btn-sm" onclick="edit(\''.$url.'\')">Edit</button>';
}

function btndel($url) {
    return '<button type="button" class="btn btn-danger btn-sm" onclick="del(\''.$url.'\')">Hapus</button>';
}

function btnadd($url) {
    return '<button type="button" class="btn btn-info btn-sm" onclick="add(\''.$url.'\')">Tambah</button>';
}

function btnback($url) {
	return '<button type="button" class="btn btn-sm" onclick="back(\''.$url.'\')">Kembali</button>';
}

function btnlinkadd($link,$title,$label){
	return '<a href="'.$link.'" class="btn btn-primary btn-sm" title="'.$title.'">'.$label.'</a>';
}