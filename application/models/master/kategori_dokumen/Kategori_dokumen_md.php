<?php
class Kategori_dokumen_md extends CI_Model {
    
    protected $entityId;
    protected $govId;
    protected $code;
    protected $suffix;
    protected $digitId;
    protected $table;
    public function __construct() {
        parent::__construct();
    }
    
    public function init( $entityId, $govId, $code )
    {
        $this->govId = $govId;
        $this->entityId = $entityId;
        $this->code = $code;
        $this->suffix = "KD";
        $this->digitId = 4;
        $this->table = "confctgr";
        
    }
    
    public function list_kategori_dokumen($startPos, $limit, $search="")
    {
        $sql = "select cbtext, cbdesc, cbuuid from ".RTRW_PROD.".".$this->table." 
            where cbentid='".$this->entityId."' and cbappid='".$this->govId."' and cbstco='".$this->code."' ";
        if (!empty($search)) {
            if (trim($search)) {
                $sql = $sql."and cbtext like '".$search."%' and cbdesc '".$search."%' ";
            }
        }
        $sql = $sql." limit ".$startPos.",".$limit;
        
        $query = $this->db->query($sql);
        return $query->result();
        
    }
    
    public function count_kategori_dokumen()
    {
        $sql = "select count(*) as cnt from ".RTRW_PROD.".".$this->table."  
        where cbentid='".$this->entityId."' and cbappid='".$this->govId."' and cbstco='".$this->code."' ";
        
        $query = $this->db->query($sql);
        $result = $query->result();
        
        return $result[0]->cnt;
    }
    
    private function new_code(){
        $query = $this->db->query("select max(cbstky) as maxStky from ".RTRW_PROD.".".$this->table." where cbappid='".$this->entityId."'");
        $rs = $query->result();
        $maxOffId = $rs[0]->maxStky;
        $numOffId = str_replace($this->suffix,"",$maxOffId);
        $numOffId = (int)$numOffId + 1;
        $maxOffId = $this->suffix.sprintf("%'.0".$this->digitId."d\n", $numOffId);
        return $maxOffId;
    }

    public function insert_kategori_dokumen($cbentid, $cbappid, $cbstco, $cbtext, $cbdesc, $userid)
    {
        $cbstky = $this->new_code();
        $this->db->query("INSERT INTO ".RTRW_PROD.".".$this->table."(CBENTID, CBAPPID, CBSTCO, CBSTKY, CBTEXT, CBDESC, CBSTAT, CBUPDT, CBUSID) 
            VALUES('".$cbentid."','".$cbappid."','".$cbstco."','".$cbstky."','".$cbtext."','".$cbdesc."',10,'".date('Y-m-d H:i:s')."','".$userid."')");
        
    }
    
    public function get_kategori_dokumen($kategoriDokumenID)
    {
        $query = $this->db->query("select * from ".RTRW_PROD.".".$this->table." 
             where cbentid='".$this->entityId."' and cbuuid='".$kategoriDokumenID."'");
        
        return $query->result();
    }
    
    public function update_kategori_dokumen($cbentid, $cbappid, $cbstco, $cbtext, $cbdesc, $userid, $cbuuid)
    {
        $this->db->query("UPDATE ".RTRW_PROD.".".$this->table." SET CBTEXT='".$cbtext."',CBDESC='".$cbdesc."', CBUPDT='".date('Y-m-d H:i:s')."', 
            CBUSID='".$userid."' where cbentid='".$this->entityId."' and cbappid='".$this->govId."' and cbuuid=".$cbuuid."");
        
    }
    
    public function delete_kategori_dokumen($kategoriDokumenID) {
        $this->db->query("delete from ".RTRW_PROD.".".$this->table." where cbuuid='".$kategoriDokumenID."'");
    }
    
}
