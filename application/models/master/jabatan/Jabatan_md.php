<?php
class Jabatan_md extends CI_Model {
    
    protected $entityId;
    protected $govId;
    protected $code;
    protected $suffix;
    protected $digitId;
    protected $table;
    protected $appid;
    
    public function __construct() {
        parent::__construct();
    }
    
    public function init( $appid, $govId, $code )
    {
        $this->govId = $govId;
       
        $this->appid = $appid;
        $this->code = $code;
        $this->suffix = "JB";
        $this->digitId = 4;
        $this->table = "confctgr";
        
    }
    
    public function list_jabatan($startPos, $limit, $search="")
    {
        $sql = "select cbstky, cbtext, cbdesc, cbuuid from ".RTRW_PROD.".".$this->table." 
            where cbentid='".$this->govId."' and cbappid='".$this->appid."' and cbstco='".$this->code."' ";
        //echo $sql;
        //break;
        if (!empty($search)) {
            if (trim($search)) {
                $sql = $sql."and cbtext like '".$search."%' and cbdesc '".$search."%' ";
            }
        }
        $sql = $sql." limit ".$startPos.",".$limit;
        $query = $this->db->query($sql);
        return $query->result();
        
    }
    
    public function count_jabatan()
    {
        $sql = "select count(*) as cnt from ".RTRW_PROD.".".$this->table."  
        where cbentid='".$this->govId."' and cbappid='".$this->appid."' and cbstco='".$this->code."' ";
        
        $query = $this->db->query($sql);
        $result = $query->result();
        
        return $result[0]->cnt;
    }
    
    public function get_jabatan($jabatanID)
    {
        $query = $this->db->query("select * from ".RTRW_PROD.".".$this->table."
             where cbentid='".$this->govId."' and cbuuid='".$jabatanID."'");
        
        return $query->result();
    }
    
    private function new_code(){
        $query = $this->db->query("select max(cbstky) as maxStky from ".RTRW_PROD.".".$this->table." where cbappid='".$this->appid."'");
        $rs = $query->result();
        $maxOffId = $rs[0]->maxStky;
        $numOffId = str_replace($this->suffix,"",$maxOffId);
        $numOffId = (int)$numOffId + 1;
        $maxOffId = $this->suffix.sprintf("%'.0".$this->digitId."d\n", $numOffId);
        return $maxOffId;
    }

    public function insert_jabatan($cbentid, $cbappid, $cbstco, $cbtext, $cbdesc, $userid)
    {
        $cbstky = $this->new_code();
        $this->db->query("INSERT INTO ".RTRW_PROD.".".$this->table."(CBENTID, CBAPPID, CBSTCO, CBSTKY, CBTEXT, CBDESC, CBSTAT, CBUPDT, CBUSID) 
            VALUES('".$cbentid."','".$cbappid."','".$cbstco."','".$cbstky."','".$cbtext."','".$cbdesc."',10,'".date('Y-m-d H:i:s')."','".$userid."')");
        
    }
    
    public function update_jabatan($cbentid, $cbappid, $cbstco, $cbtext, $cbdesc, $userid, $cbuuid)
    {
        
        $this->db->query("UPDATE ".RTRW_PROD.".".$this->table." SET CBSTKY='".$cbstky."', CBTEXT='".$cbtext."',CBDESC='".$cbdesc."', CBUPDT='".date('Y-m-d H:i:s')."', 
            CBUSID='".$userid."' where cbentid='".$this->govId."' and cbappid='".$this->appid."' and cbuuid=".$cbuuid."");
        
    }
    
    public function delete_jabatan($jabatanID) {
        $this->db->query("delete from ".RTRW_PROD.".".$this->table." where cbuuid='".$jabatanID."'");
    }
    
}
