<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Entity extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    
    function show() {
        $query = $this->db->query('SELECT MSENTID , MSTYPE, GPGOVNM FROM common_prod.MSTRENTY Left Join rtrw_prod.GOVPRFL on MSENTID=GPGOVID');
        $result = "";
        foreach ($query->result() as $row)
        {
            $result = $result.$row->MSENTID."<br>";
            $result = $result.$row->MSTYPE."<br>";
            $result = $result.$row->GPGOVNM."<br>";
           
        }
        
        $result = $result.' Total Results: ' . $query->num_rows();
       
        
        return $result;
    }
}