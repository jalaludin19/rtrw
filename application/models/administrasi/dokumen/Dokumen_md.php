<?php
class Dokumen_md extends CI_Model {
    
    protected $govId;
    protected $table;
	public function __construct() {
		parent::__construct();
	}
	public function init( $govId )
	{	    
	    $this->govId = $govId;
	    $this->table = "govdcmnt";
	}
	public function list_dokumen($startPos, $limit, $search="")
	{
	    $sql = "select gdtitl, gdflnm, gdnote, '' as gdctgr 
            from ".RTRW_PROD.".".$this->table."
            where gdgovid='".$this->govId."' ";
	    if (!empty($search)) {
	        if (trim($search)) {
	            $sql = $sql."and gdtitl like '".$search."%' and gdflnm '".$search."%' ";
	        }
	    }
	    $sql = $sql." limit ".$startPos.",".$limit;
	    
	    $query = $this->db->query($sql);
	    return $query->result();
	    
	}
	public function count_dokumen()
	{
	    $sql = "select count(*) as cnt from ".RTRW_PROD.".".$this->table."
        where gdgovid='".$this->govId."' ";
	    
	    $query = $this->db->query($sql);
	    $result = $query->result();
	    
	    return $result[0]->cnt;
	}
	

}
