<?php
class pengumuman_md extends CI_Model {
    protected $govId;
    protected $suffix;
    protected $digitId;
    public function __construct() {
        parent::__construct();
    }
    
    public function init( $govId )
    {
        $this->govId = $govId;
        $this->usid = "ugi";
        $this->suffix = "PG";
        $this->digitId = 4;
        
    }
    public function save_pengumuman($data_s){
	//PMAAUID
				
        $this->db->query("INSERT INTO ".RTRW_PROD.".GVANCMT (PMGOVID,PMASTDT,PMAENDT,PMASBJC,PMADESC,PMASTAT,PMARGDT,PMAUPDT,PMAUSID) 
        values ('".$this->govId."','".$data_s['pmastdt']."','".$data_s['pmaendt']."','".$data_s['pmasbjc']."',
                '".$data_s['pmadesc']."','20',NOW(),NOW(),'".$data_s['usid']."')");
        
    }
    
    public function update_pengumuman($data_s){
        
        $this->db->query("UPDATE  ".RTRW_PROD.".GVANCMT SET PMASTDT='".$data_s['pmastdt']."',PMAENDT='".$data_s['pmaendt']."',PMASBJC='".$data_s['pmasbjc']."',
                           PMADESC='".$data_s['pmadesc']."',PMAUPDT=NOW(),PMAUSID='".$data_s['usid']."' WHERE PMUUID='".$data_s['pmuuid']."'");
    }
    
    public function delete_pengumuman($data_s,$pmmauid){
        echo "UPDATE  ".RTRW_PROD.".GVANCMT SET PMASTAT='99',PMAUPDT=NOW(),PMAUSID='".$data_s['usid']."' WHERE PMUUID='".$pmmauid."'";
        $this->db->query("UPDATE  ".RTRW_PROD.".GVANCMT SET PMASTAT='99',PMAUPDT=NOW(),PMAUSID='".$data_s['usid']."' WHERE PMUUID='".$pmmauid."'  ");
    }
        
    public function get_pengumuman($pmmauid){
       
        $query =$this->db->query("SELECT pmuuid,pmgovid,pmasbjc,pmadesc,pmastdt,pmaendt FROM ".RTRW_PROD.".GVANCMT WHERE PMUUID='".$pmmauid."' AND PMGOVID='".$this->govId."'  ");
        return $query->result();
    }
    
   
    public function list_pengumuman(){
        $query =$this->db->query("SELECT pmuuid,pmgovid,pmasbjc,pmadesc,pmastdt,pmaendt FROM ".RTRW_PROD.".GVANCMT WHERE PMGOVID='".$this->govId."' ");
       	return $query->result();
    }
    
}