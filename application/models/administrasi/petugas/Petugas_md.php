<?php
class Petugas_md extends CI_Model {
    
    protected $govId;
    protected $suffix;
    protected $digitId;
    public function __construct() {
        parent::__construct();
    }
    
    public function init( $govId )
    {
        $this->govId = $govId;
        $this->suffix = "PG";
        $this->digitId = 4;
        
    }
    
    public function list_petugas($startPos, $limit, $search="")
    {
        $sql = "select ofoffid, ofprnam, ofactiv, ofposid, ofdatfr, ofuuid from ".RTRW_PROD.".GVOFICER where OFGOVID='".$this->govId."' ";
        if (!empty($search)) {
            if (trim($search)<>"") {
                $sql = $sql." and ofprnam like '".$search."%'";
            }
        }
        $sql = $sql." and ofstat<99 limit ".$startPos.",".$limit;        
        $query = $this->db->query($sql);        
        return $query->result();
    }
    
    public function count_petugas()
    {
        $sql = "select count(*) as cnt from ".RTRW_PROD.".GVOFICER where OFGOVID='".$this->govId."' ";        
        $query = $this->db->query($sql);
        $result = $query->result();        
        return $result[0]->cnt;
    }
    
    public function get_petugas($id)
    {
        $query = $this->db->query("select ofoffid, ofprnam, ofposid,ofprnik,ofaddrs,ofcity,ofdatfr,ofdatto,ofactiv, ofuuid from ".RTRW_PROD.".GVOFICER where OFUUID='".$id."'");
        
        return $query->result();
    }
    
    private function new_posisiId() {
        $query = $this->db->query("select max(OFOFFID) as maxOffId from ".RTRW_PROD.".GVOFICER where OFGOVID='".$this->govId."'");
        $rs = $query->result();
        $maxOffId = $rs[0]->maxOffId;
        $numOffId = str_replace($this->suffix,"",$maxOffId);
        $numOffId = (int)$numOffId + 1;
        $maxOffId = $this->suffix.sprintf("%'.0".$this->digitId."d\n", $numOffId);
        return $maxOffId;
        
    }
    public function insert_petugas($posisiId, $nik, $name, $alamat, $kota, $tglmulai, $tglakhir, $aktif, $usid )
    {
        if (trim($tglakhir)=="") $tglakhir="null";
        
        $petugasId = $this->new_posisiId();
        $this->db->query("insert into ".RTRW_PROD.".GVOFICER ( OFGOVID, OFOFFID, OFPOSID, OFPRNIK, OFPRNAM, OFADDRS,
            OFCITY, OFDATFR, OFDATTO, OFACTIV, OFSTAT, OFUPDT, OFUSID) values ('".$this->govId."','".$petugasId."','".$posisiId."',
            '".$nik."','".$name."','".$alamat."','".$kota."','".$tglmulai."','".$tglakhir."','".$aktif."',10,NOW(),'".$usid."')");
    }
    
    public function update_petugas($id, $posisiId, $nik, $name, $alamat, $kota, $tglmulai, $tglakhir, $aktif, $usid )
    {
        if (trim($tglakhir)=="") $tglakhir="null";
        $this->db->query("update ".RTRW_PROD.".GVOFICER set OFPOSID='".$posisiId."', OFPRNIK='".$nik."',
            OFPRNAM='".$name."', OFADDRS='".$alamat."', OFCITY='".$kota."', 
            OFDATFR='".$tglmulai."',OFDATTO='".$tglakhir."', OFACTIV='".$aktif."',
            OFUPDT=NOW(),  OFUSID='".$usid."'
            where OFUUID=".$id);
    }
    
    public function delete_petugas($id) {
        $this->db->query("delete from ".RTRW_PROD.".GVOFICER where OFUUID=".$id);
    }
    
}
