<?php
class Daftarlain_md extends CI_Model {
    protected $govId;
   
    public function __construct() {
        parent::__construct();
    }
    
    public function init( $govId )
    {
        $this->govId = $govId;
    }
    
    public function list_daftarlain($startPos, $limit, $search="")
    {
        $sql = "select lslstid, lslname, lsdescr, lsuuid from ".RTRW_PROD.".LISTVARI where LSGOVID='".$this->govId."' ";
        if (!empty($search)) {
            if (trim($search)<>"") {
                $sql = $sql." and (lslistn like '".$search."%' or lsdescr like '".$search."%') ";
            }
        }
        $sql = $sql." and lsstat<99 limit ".$startPos.",".$limit;        
        $query = $this->db->query($sql);        
        return $query->result();
    }
    
    public function get_daftarlain($id)
    {
        $query = $this->db->query("select lslstid, lslname, lsdescr from ".RTRW_PROD.".LISTVARI where LSUUID='".$id."'");
        
        return $query->result();
    }
    
    public function get_daftarlainLstid($id)
    {
        $query = $this->db->query("select lslstid, lslname, lsdescr from ".RTRW_PROD.".LISTVARI where LSGOVID='".$this->govId."' and LSLSTID='".$id."'");
        
        return $query->result();
    }
    
    
    public function count_daftarlain()
    {
        $sql = "select count(*) as cnt from ".RTRW_PROD.".LISTVARI where LSGOVID='".$this->govId."' ";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result[0]->cnt;
    }
    
    private function new_listid() {
        $query = $this->db->query("select max(LSLSTID) as maxLstID from ".RTRW_PROD.".LISTVARI where LSGOVID='".$this->govId."'");
        $rs = $query->result();
        $maxLstID = $rs[0]->maxLstID;
        $result = (int)$maxLstID + 1;       
        return $result;
    }
    
    public function insert_daftarlain($name, $descr, $usid )
    {
        $listid = $this->new_listid();
        $this->db->query("insert into ".RTRW_PROD.".LISTVARI ( LSGOVID, LSLSTID, LSLNAME, LSDESCR, LSSTAT, LSUPDT,
            LSUSID) values ('".$this->govId."','".$listid."','".$name."',
            '".$descr."',10,NOW(),'".$usid."')");
    }
    
    public function update_daftarlain($id, $name, $descr, $usid )
    {
        
        $this->db->query("update ".RTRW_PROD.".LISTVARI set LSLNAME='".$name."', LSDESCR='".$descr."',
            LSUPDT=NOW(), LSUSID='".$usid."'
            where LSUUID=".$id);
    }
    
    public function delete_daftarlain($id) {
        $this->db->query("delete from ".RTRW_PROD.".LISTVARI where LSGOVID='".$this->govId."' and LSLSTID='".$id."'");
        $this->db->query("delete from ".RTRW_PROD.".LISTCOLM where LCGOVID='".$this->govId."' and LCLSTID='".$id."'");
    }
    
}