<?php
class Columns_md extends CI_Model {
    protected $govId;
    private $listId;
    public function __construct() {
        parent::__construct();
    }
    
    public function init( $govId )
    {
        $this->govId = $govId;
        
    }
    
    public function setListId( $listId) {
        $this->listId = $listId;
    }
    public function list_column( $search="")
    {
        $sql = "select lccolid, lccolnm, lcuuid from ".RTRW_PROD.".LISTCOLM where LCGOVID='".$this->govId."' and LCLSTID='".$this->listId."' ";
        if (!empty($search)) {
            if (trim($search)) {
                $sql = $sql." and lccolnm like '".$search."%'";
            }
        }
        $sql = $sql." order by lccolid ";
       
        $query = $this->db->query($sql);
        return $query->result();
    }
   
    public function count_column()
    {
        $sql = "select count(*) as cnt from ".RTRW_PROD.".LISTCOLM where LCGOVID='".$this->govId."' and LCLSTID='".$this->listId."'";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result[0]->cnt;
    }
    
    private function new_colid() {
        $query = $this->db->query("select max(LCCOLID) as maxColId from ".RTRW_PROD.".LISTCOLM where LCGOVID='".$this->govId."' and LCLSTID='".$this->listId."'");
        $rs = $query->result();
        $maxColId = $rs[0]->maxColId;
        $result = (int)$maxColId + 1;
        return $result;
    }
    
    public function insert_column($colname )
    {
        $colId = $this->new_colid();
        $this->db->query("insert into ".RTRW_PROD.".LISTCOLM ( LCCOLID, LCGOVID, LCLSTID, LCCOLNM) 
        values (".$colId.",'".$this->govId."','".$this->listId."','".$colname."')");
    }
    
    public function delete_column($id) {
        $this->db->query("delete from ".RTRW_PROD.".LISTCOLM where LCUUID=".$id);
    }
    
    public function get_column($id) {
        $query = $this->db->query("select lccolnm from ".RTRW_PROD.".LISTCOLM where LCUUID='".$id."'");
        
        return $query->result();
    }
    
    public function update_column($id, $colname) {
        $this->db->query("update ".RTRW_PROD.".LISTCOLM set lccolnm='".$colname."' where LCUUID='".$id."'");
        
    }
    
}