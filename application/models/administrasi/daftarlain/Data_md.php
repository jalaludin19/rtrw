<?php
class Data_md extends CI_Model {
    protected $govId;
    private $listId;
    public function __construct() {
        parent::__construct();
    }
    
    public function init( $govId )
    {
        $this->govId = $govId;        
    }
    
    public function setListId( $listId) {
        $this->listId = $listId;
    }
    public function list_data($startPos, $limit, $search="")
    {
        $sql = "select ldgovid, ldlstid, lddtid, ldcolid, ldtext, lduuid from ".RTRW_PROD.".LISTDATA
             where LDGOVID='".$this->govId."' and LDLSTID='".$this->listId."' ";
        if (!empty($search)) {
            if (trim($search)<>"") {
                $sql = $sql." and ldtext like '".$search."%'";
            }
        }
        $sql = $sql." order by lddtid ";
        $sql = $sql." limit ".$startPos.",".$limit;
        //echo $sql;
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    public function count_data()
    {
        $sql = "select count(*) as cnt from ".RTRW_PROD.".LISTDATA where LDGOVID='".$this->govId."' and LDLSTID='".$this->listId."'";
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result[0]->cnt;
    }
    
    public function new_dtid() {
        $query = $this->db->query("select max(LDDTID) as maxDatId from ".RTRW_PROD.".LISTDATA where LDGOVID='".$this->govId."' and LDLSTID='".$this->listId."'");
        $rs = $query->result();
        $maxDatId = $rs[0]->maxDatId;
        $result = (int)$maxDatId + 1;
        return $result;
    }
    
    public function insert_data($datId, $colid, $text, $usid )
    {              
        $this->db->query("insert into ".RTRW_PROD.".LISTDATA ( LDGOVID, LDLSTID, LDCOLID, LDDTID, LDTEXT, LDUPDT, LDUSID)
        values ('".$this->govId."','".$this->listId."',".$colid.",".$datId.",'".$text."',curdate(),'".$usid."')");
    }
    
    public function delete_data($datId) {
        $this->db->query("delete from ".RTRW_PROD.".LISTDATA where ldgovid='".$this->govId."' and ldlstid=".$this->listId." and LDDTID=".$datId);
    }
    
    public function get_data($datId) {
        $query = $this->db->query("select ldcolid, ldtext from ".RTRW_PROD.".LISTDATA where ldgovid='".$this->govId."' and ldlstid=".$this->listId." and LDDTID='".$datId."'");
                
        return $query->result();
    }
    
    public function update_data($datId, $colid, $text, $usid) {
        $this->db->query("update ".RTRW_PROD.".LISTDATA set ldtext='".$text."', ldusid='".$usid."' 
            where ldgovid='".$this->govId."' and ldlstid=".$this->listId." and LDDTID='".$datId."' and ldcolid=".$colid);
        
    }
    
}