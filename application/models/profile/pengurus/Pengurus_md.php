<?php
class Pengurus_md extends CI_Model {
    
    protected $govId;
    protected $suffix;
    protected $digitId;
    public function __construct() {
        parent::__construct();
    }
    
    public function init( $govId )
    {
        $this->govId = $govId;
        $this->suffix = "PG";
        $this->digitId = 4;
        
    }
    
    public function list_pengurus($startPos, $limit, $search="")
    {
        $sql = "select mgoffid, mgprnam, mgactiv, mgposid, mgdatfr, mguuid from ".RTRW_PROD.".GVMNGMNT where mgGOVID='".$this->govId."' ";
        if (!empty($search)) {
            if (trim($search)<>"") {
                $sql = $sql." and mgprnam like '".$search."%'";
            }
        }
        $sql = $sql." and mgstat<99 limit ".$startPos.",".$limit;        
        $query = $this->db->query($sql);        
        return $query->result();
    }
    
    public function count_pengurus()
    {
        $sql = "select count(*) as cnt from ".RTRW_PROD.".GVMNGMNT where mgGOVID='".$this->govId."' ";        
        $query = $this->db->query($sql);
        $result = $query->result();        
        return $result[0]->cnt;
    }
    
    public function get_pengurus($id)
    {
        $query = $this->db->query("select mgoffid, mgprnam, mgposid,mgprnik,mgaddrs,mgcity,mgdatfr,mgdatto,mgactiv, mguuid from ".RTRW_PROD.".GVMNGMNT where mgUUID='".$id."'");
        
        return $query->result();
    }
    
    private function new_posisiId() {
        $query = $this->db->query("select max(mgOFFID) as maxOffId from ".RTRW_PROD.".GVMNGMNT where mgGOVID='".$this->govId."'");
        $rs = $query->result();
        $maxOffId = $rs[0]->maxOffId;
        $numOffId = str_replace($this->suffix,"",$maxOffId);
        $numOffId = (int)$numOffId + 1;
        $maxOffId = $this->suffix.sprintf("%'.0".$this->digitId."d\n", $numOffId);
        return $maxOffId;
        
    }
    public function insert_pengurus($posisiId, $nik, $name, $alamat, $kota, $tglmulai, $tglakhir, $aktif, $usid )
    {
        if (trim($tglakhir)=="") $tglakhir="null";
        
        $petugasId = $this->new_posisiId();
        $this->db->query("insert into ".RTRW_PROD.".GVMNGMNT ( mgGOVID, mgOFFID, mgPOSID, mgPRNIK, mgPRNAM, mgADDRS,
            mgCITY, mgDATFR, mgDATTO, mgACTIV, mgSTAT, mgUPDT, mgUSID) values ('".$this->govId."','".$petugasId."','".$posisiId."',
            '".$nik."','".$name."','".$alamat."','".$kota."','".$tglmulai."','".$tglakhir."','".$aktif."',10,NOW(),'".$usid."')");
    }
    
    public function update_pengurus($id, $posisiId, $nik, $name, $alamat, $kota, $tglmulai, $tglakhir, $aktif, $usid )
    {
        if (trim($tglakhir)=="") $tglakhir="null";
        $this->db->query("update ".RTRW_PROD.".GVMNGMNT set mgPOSID='".$posisiId."', mgPRNIK='".$nik."',
            mgPRNAM='".$name."', mgADDRS='".$alamat."', mgCITY='".$kota."', 
            mgDATFR='".$tglmulai."',mgDATTO='".$tglakhir."', mgACTIV='".$aktif."',
            mgUPDT=NOW(),  mgUSID='".$usid."'
            where mgUUID=".$id);
    }
    
    public function delete_pengurus($id) {
        $this->db->query("delete from ".RTRW_PROD.".GVMNGMNT where mgUUID=".$id);
    }
    
}
