<?php
class Info_md extends CI_Model {

	 
	public function __construct() {
		parent::__construct();
	}
	public function get_entities()
	{
		$query = $this->db->query('select * from common_prod.mstrenty');
		return $query->result();
	}

	public function get_details($entityId)
	{
		$query = $this->db->query("select * from common_prod.mstrenty where MSENTID='".$entityId."'");
		return $query->result();
	}


	public function insert_entity($entityId, $type)
	{
		$this->db->query("insert into common_prod.mstrenty (MSENTID, MSTYPE) values ('".$entityId."','".$type."')");
	}

	public function update_entity($entityId, $type)
	{
		$this->db->query("update common_prod.mstrenty set MSTYPE='".$type."' where MSENTID='".$entityId."'");
	}

	public function delete_entity($entityId) {
		$this->db->query("delete from common_prod.mstrenty where MSENTID='".$entityId."'");
	}
	
	public function get_provinces(){
		$query = $this->db->query("select id as pvid ,name as pvname from common_prod.provinces");	
		return $query->result();
	}
	
	public function get_regencies($id){
	    if (!empty($id)){
	        
	        $prvid = " where province_id ='".$id."' ";
	        
	    }
	    else{
	        $prvid ="";
	    }
		$query = $this->db->query("select id as rgid ,name as rgname from common_prod.regencies  $prvid ");
		
		//return $query->result();
		return $query->result();
	}
	
	public function get_district($id){
	    if (!empty($id)){
	        
	        $regid = " where regency_id ='".$id."' ";
	        
	    }
	    else{
	        $regid ="";
	    }
	    $query = $this->db->query("select id as dsid ,name as dsname from common_prod.districts $regid ");
	    
	    //echo $query;
	    return $query->result();
	}
	public function get_villages($id){
	    if (!empty($id)){
	        
	        $disid = " where district_id ='".$id."' ";
	        
	    }
	    else{
	        $disid ="";
	    }
	    $query = $this->db->query("select id as vlid ,name as vlname from common_prod.villages $disid ");
	    
	    //return $query->result();
	    return $query->result();
	}
	
    
	//simpan profile 
	public function save_profile($data){
	    //$check =  "select count(*) as ada from rtrw_prod.gvprofil";
	    //$rs_check = $this->db->query($check);
	    //var_dump($rs_check->row_array());
	    //return $rs_check;
	    /*if ($rs_check->ada == 0){*/
	    return  $this->db->query(
	        "UPDATE rtrw_prod.gvprofil SET 
            GPPRVID= '".$data["GPPRVID"]."',
            GPRGCID= '".$data["GPRGCID"]."',
            GPDISID= '".$data["GPDISID"]."',
            GPVILID= '".$data["GPVILID"]."',
            GPGOVLV= '2',
            GPRTNUM=  '".$data["GPRTNUM"]."',
            GPRWNUM=  '".$data["GPRWNUM"]."',
            GPADDRS= '".$data["GPADDRS"]."',
			GPPOSCD= '".$data["GPPOSCD"]."',
			GPPHONE= '".$data["GPPHONE"]."',
			GPEMAIL= '".$data["GPEMAIL"]."',
			GPRTNM = '".$data["GPRTNM"]."',
			GPRWNM = '".$data["GPRWNM"]."',
            GPREGID= 'ADMIN',
            GPREGDT= NOW(),
            GPFLNM = '".$data["GPFLNM"]."'
            WHERE GPGOVID ='1' ");
	        //$query = $this->db->query($query);
	        //return $query->result();
	        //print_r($query->result());
    	   /* $query = $this->db->query("
            INSERT INTO rtrw_prod.gvprofil 
            (GPGOVID,GPPRVID,GPRGCID,GPDISID,GPVILID,GPGOVLV,GPRTNUM,GPRWNUM,GPADDRS,GPREGID,GPREGDT) 
            VALUES(
                '1',
                '".$data["GPPRVID"]."',
                '".$data["GPRGCID"]."',
                '".$data["GPDISID"]."',
                '".$data["GPVILID"]."',
                '2',
                '".$data["GPRTNUM"]."',
                '".$data["GPRWNUM"]."',
                '".$data["GPADDRS"]."',
                'ADMIN',
                NOW()
                
                )"
    	    );*/
    	    
	   // }
	    //else{
	        
	        //echo "update";
	    //}*/
	    //echo $query; 
	    //return $query->result();
	    
	}
	
	public function getProfileInfo($id){
	    $query = $this->db->query("select GPPRVID,GPRGCID,GPDISID,GPVILID,GPRTNUM,GPRWNUM,GPFLNM,
        GPADDRS,GPPOSCD,GPPHONE,GPEMAIL,GPRTNM,GPRWNM, a.name as propinsi,b.name as kabupaten,c.name as kecamatan,d.name as kelurahan
        from rtrw_prod.gvprofil 
        left join common_prod.provinces a on a.id = gpprvid 
        left join common_prod.regencies b on b.id = gprgcid
        left join common_prod.districts c on c.id = gpdisid
        left join common_prod.villages d on d.id = gpvilid

        where gpgovid ='".$id."' ");
	    //echo $query;
	    return $query->result();
	    
	
	}
}
