<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct() {
		parent::__construct();
		$this->load->model('Test/Test_md');
	}
	public function index()
	{
		
		$data["entity"] = $this->Test_md->get_entities();
		$this->load->view('Test/test_vw', $data);
	}
	
	public function add() {
		$data["formAction"] = "../addSave";
		
		$this->load->view('Test/testInput_vw',$data);
	}
	
	public function addSave() {
		$entityId = $_POST["entityId"];
		$type = $_POST["type"];
		
		$this->Test_md->insert_entity($entityId, $type);
		redirect('/test/test');
	}
	
	public function edit($entityId) {
		$data["formAction"] = "../editSave";
		$data["entity"] = $this->Test_md->get_details($entityId);
		
		$this->load->view('Test/testInput_vw',$data);
	}
	
	public function editSave() {
		$entityId = $_REQUEST["entityId"];
		$type = $_REQUEST["type"];
		
		$this->Test_md->update_entity($entityId, $type);
		redirect('/test/test');
	}
}
