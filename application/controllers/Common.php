<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model("Entity");
    }
    
    public function index() {
        $result = $this->Entity->show();
        $arr = array();
        $arr['result'] = $result;
        $this->load->view('common',$arr);
    }
}