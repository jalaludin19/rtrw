<?php
class Kategori_dokumen extends CI_Controller {
	private $header;
	private $govId;
	private $data;
	private $entityId;
	private $code;
	function __construct()
	{
		parent::__construct();
		$this->load->model('master/kategori_dokumen/Kategori_dokumen_md');
		$this->entityId = "1";
        $this->govId = "RTRW";
        $this->code = "KATDOK";
        $this->Kategori_dokumen_md->init($this->entityId, $this->govId, $this->code);
        
        $this->header["title"] = "Kategori Dokumen";
        $_SESSION["cntKatDok"] = $this->Kategori_dokumen_md->count_kategori_dokumen();
        $_SESSION["pageCount"] = ceil($_SESSION["cntKatDok"]/RECORD_LIMIT);
        $this->header["jspath"] = "master/kategori_dokumen/kategori_dokumen.js";
                
        $this->data["base_url"] = base_url(); 
	}
	
	private function getlist_kategori_dokumen($page, $search) {
	    
	    $startpos = ($page-1)*RECORD_LIMIT ;
	    return $this->Kategori_dokumen_md->list_kategori_dokumen($startpos, RECORD_LIMIT, $search);
	}
	
	public function index()
	{	 
	    $this->header["btndel"] = btndel('');
	    $this->header["btnadd"] = btnadd(base_url()."/master/kategori_dokumen/kategori_dokumen/add");
	    $this->header["btnlinkadd"] = btnlinkadd(base_url()."master/kategori_dokumen/kategori_dokumen/add", "Tambah Kategori Dokumen", "Tambah Kategori Dokumen");
	    		
	    $data["govId"] = $this->govId;
	    $data["pageCount"] = $_SESSION["pageCount"];
	    
	    $page = 1;
	    if (isset($_POST["search"])) {
	        $search = $_POST["search"];
	    } else {
	        $search ="";
	    }
	    $data["lstKatDok"] = $this->getlist_kategori_dokumen($page, $search );
	    
	    $this->load->view('header',$this->header);
	    $this->load->view('master/kategori_dokumen/kategori_dokumen_vw', $data);
	    $this->load->view('footer');
	}
	
	public function page($page) {
	    $this->header["btndel"] = btndel();
	    $data["pageCount"] = $_SESSION["pageCount"];
	    if (isset($_POST["search"])) {
	        $search = $_POST["search"];
	    } else {
	        $search ="";
	    }
	    $data["lstKatDok"] = $this->getlist_kategori_dokumen($page, $search );
	    $this->load->view('header',$this->header);
	    $this->load->view('master/kategori_dokumen/kategori_dokumen_vw', $data);
	    $this->load->view('footer');
	}	
	
	public function add(){
		$this->header["btnsave"] = btnsave("");
		$this->header["btnback"] = btnback(base_url()."/master/kategori_dokumen/kategori_dokumen");
		
		$this->data["govId"] = $this->govId;
		$this->data["code"] = $this->code;
		$this->data["uuid"] = '';
		$this->data["formAction"] = "addSave";
		
		$this->load->view('header',$this->header);
		$this->load->view('master/kategori_dokumen/kategori_dokumenInput_vw',$this->data);
		$this->load->view('footer');
	}
	
	public function addSave(){	    
	    $cbtext  = $this->input->post("cbtext");
	    $cbdesc  = $this->input->post("cbtext");
	    $usid = 'ADMIN';
	    
	    $this->Kategori_dokumen_md->insert_kategori_dokumen($this->entityId, $this->govId, $this->code, $cbtext, $cbdesc, $usid);
	    redirect('/master/kategori_dokumen/kategori_dokumen');
	}
	
	public function edit($cbuuid) {
	    $this->header["btnsave"] = btnsave("");
	    $this->header["btnback"] = btnback(base_url()."/master/kategori_dokumen");
	    
	    $data["govId"] = $this->govId;
	    $data["uuid"] = $cbuuid;
	    $data["formAction"] = "editSave";
	    
	    
	    $data["dtlKatDok"] = $this->Kategori_dokumen_md->get_kategori_dokumen($cbuuid);
	    $this->load->view('header',$this->header);
	    $this->load->view('master/kategori_dokumen/kategori_dokumenInput_vw',$data);
	    $this->load->view('footer');
	}
	
	public function editSave(){
	    $cbtext  = $this->input->post("cbtext");
	    $cbdesc  = $this->input->post("cbdesc");
	    $cbuuid = $this->input->post("cbuuid");
	    $usid = 'ADMIN';
	    
	    $this->Kategori_dokumen_md->update_kategori_dokumen($this->entityId, $this->govId, $this->code, $cbtext, $cbdesc, $usid, $cbuuid);
	    redirect('/master/kategori_dokumen/kategori_dokumen/show/'.$cbuuid);
	}
	
	public function show($cbuuid) {
	    $this->header["btnadd"] = btnadd(base_url()."/master/kategori_dokumen/kategori_dokumen/add");
	    
	    $this->header["btnback"] = btnback(base_url()."/master/kategori_dokumen/kategori_dokumen");
	    
	    $data["govId"] = $this->govId;
	    $data["uuid"] = $cbuuid;
	    
	    $data["dtlKatDok"] = $this->Kategori_dokumen_md->get_kategori_dokumen($cbuuid);
	    $this->load->view('header',$this->header);
	    $this->load->view('master/kategori_dokumen/kategori_dokumenShow_vw',$data);
	    $this->load->view('footer');
	}
	
	public function delete($id) {
	    
	    $this->Kategori_dokumen_md->delete_kategori_dokumen($id);
	    redirect('/master/kategori_dokumen/kategori_dokumen');
	}
	
	public function deletemany() {
	    
	    $arr = $_POST["id"];
	    foreach ($arr as $id) {
	        $this->Kategori_dokumen_md->delete_kategori_dokumens($id);
	    }
	    redirect('/master/kategori_dokumen/Kategori_dokumen');
	}
	
}

?>