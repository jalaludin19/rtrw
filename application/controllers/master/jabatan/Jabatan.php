<?php
class Jabatan extends CI_Controller {    
    private $header;
    private $govId;
    private $data;
    private $entityId;
    private $code;
    
	function __construct()
	{
	    parent::__construct();
	    $this->load->model('master/jabatan/Jabatan_md');
	    $this->entityId = "1";
	    $this->appId = "RTRW";
	    $this->code = "JABATN";
	    $this->Jabatan_md->init($this->appId, $this->entityId, $this->code);
	    
	    $this->header["title"] = "Jabatan";
	    $_SESSION["cntJabatan"] = $this->Jabatan_md->count_jabatan();
	    $_SESSION["pageCount"] = ceil($_SESSION["cntJabatan"]/RECORD_LIMIT);
	    $this->header["jspath"] = "master/jabatan/jabatan.js";
	    
	    $this->data["base_url"] = base_url(); 
	}
	public function index()
	{		
	    $this->header["btndel"] = btndel('');
	    $this->header["btnadd"] = btnadd(base_url()."master/jabatan/jabatan/add");
	    $this->header["btnlinkadd"] = btnlinkadd(base_url()."master/jabatan/jabatan/add", "Tambah Jabatan", "Tambah Jabatan");
	    
	    $data["govId"] = $this->entityId;
	    $data["pageCount"] = $_SESSION["pageCount"];
	    
	    $page = 1;
	    if (isset($_POST["search"])) {
	        $search = $_POST["search"];
	    } else {
	        $search ="";
	    }
	    $data["lstJabatan"] = $this->getlist_jabatan($page, $search );
	    
	    $this->load->view('header',$this->header);
	    $this->load->view('master/jabatan/jabatan_vw', $data);
	    $this->load->view('footer');
	}
	
	private function getlist_jabatan($page, $search) {	    
	    $startpos = ($page-1)*RECORD_LIMIT ;
	    return $this->Jabatan_md->list_jabatan($startpos, RECORD_LIMIT, $search);
	}
	
	public function add(){
		$this->header["btnsave"] = btnsave("");
		$this->header["btnback"] = btnback(base_url()."master/jabatan/jabatan");
		$this->data["uuid"] = '';
		$this->data["govId"] = $this->entityId;
		$this->data["formAction"] = "addSave";
	
		$this->load->view('header',$this->header);
		$this->load->view('master/jabatan/jabatanInput_vw',$this->data);
		$this->load->view('footer');
	}
	
	public function addSave(){
	    $cbtext  = $this->input->post("cbtext");
	    $cbdesc  = $this->input->post("cbdesc");
	    $usid = 'ADMIN';
	    
	    $this->Jabatan_md->insert_jabatan($this->entityId, $this->appId, $this->code, $cbtext, $cbdesc, $usid);
	    redirect('/master/jabatan/jabatan');
	}
	
	public function edit($cbuuid) {
	    $this->header["btnsave"] = btnsave("");
	    $this->header["btnback"] = btnback(base_url()."/master/jabatan/jabatan");
	    
	    $data["govId"] = $this->entityId;
	    $data["uuid"] = $cbuuid;
	    $data["formAction"] = "editSave";
	    
	    
	    $data["dtljabatan"] = $this->Jabatan_md->get_jabatan($cbuuid);
	    $this->load->view('header',$this->header);
	    $this->load->view('master/jabatan/jabatanInput_vw',$data);
	    $this->load->view('footer');
	}
	
	public function editSave(){
	    $cbtext  = $this->input->post("cbtext");
	    $cbdesc  = $this->input->post("cbdesc");
	    $cbuuid = $this->input->post("cbuuid");
	    $usid = 'ADMIN';
	    
	    $this->Jabatan_md->update_jabatan($this->entityId, $this->entityId, $this->code, $cbtext, $cbdesc, $usid, $cbuuid);
	    redirect('/master/jabatan/jabatan/show/'.$cbuuid);
	}
	
	public function show($cbuuid) {
	    $this->header["btnadd"] = btnadd(base_url()."/master/jabatan/jabatan/add");
	    
	    $this->header["btnback"] = btnback(base_url()."/master/jabatan/jabatan");
	    
	    $data["govId"] = $this->entityId;
	    $data["uuid"] = $cbuuid;
	    
	    $data["dtljabatan"] = $this->Jabatan_md->get_jabatan($cbuuid);
	    $this->load->view('header',$this->header);
	    $this->load->view('master/jabatan/jabatanShow_vw',$data);
	    $this->load->view('footer');
	}
	
	public function delete($cbuuid) {	    
	    $this->Jabatan_md->delete_jabatan($cbuuid);
	    redirect('/master/jabatan/jabatan');
	}
	
	public function deletemany() {
	    
	    $arr = $_POST["id"];
	    foreach ($arr as $id) {
	        $this->Jabatan_md->delete_jabatan($id);
	    }
	    redirect('/master/kategori_dokumen/Kategori_dokumen');
	}
	
}

?>