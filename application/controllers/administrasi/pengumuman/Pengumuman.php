<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengumuman extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('administrasi/pengumuman/Pengumuman_md');
        $this->govId = "RT0001";
        $this->Pengumuman_md->init($this->govId);
        $this->header["jspath"] = "administrasi/pengumuman/pengumuman.js";
        
    }

    public function index()
    {
        //$this->header["btndel"] = btndel('');
        $data["lstpengumuman"] = $this->Pengumuman_md->list_pengumuman();
        
        $this->load->view('header',$this->header);
        $this->load->view('administrasi/pengumuman/pengumuman_vw',$data);
        $this->load->view('footer');
        
    }
    
    public function add_()
    {
        
        $header["title"] = "Pengumuman";
        $header["btnsave"] = btnsave('');
        $header["btnedit"] = btnedit('');
        $header["btndel"] = btndel('');
        $header["btnadd"] = btnadd('');
        $header["jspath"] = "administrasi/pengumuman/pengumuman.js";
        
        
        $this->load->view('header',$header);
        $this->load->view('administrasi/pengumuman/pengumuman_vw');
        $this->load->view('footer');
    }
    
    public function add() {
        $this->header["btnsave"] = btnsave('');
        
        $data["govId"] = $this->govId;
        $data["formAction"] = "addSave";
        $data["lstpengumuman"] = $this->Pengumuman_md->list_pengumuman();
        
        $this->load->view('header',$this->header);
        $this->load->view('administrasi/pengumuman/pengumumanInput_vw',$data);
        $this->load->view('footer');
    }
    
    public function Show($pmuuid) {
        $this->header["btnback"] = btnback(base_url()."/administrasi/pengumuman/pengumuman");
        
        $data["govId"] = $this->govId;
        $data["formAction"] = "addSave";
        $data["pmuuid"] = $pmuuid;
        $data["getpengumuman"] = $this->Pengumuman_md->get_pengumuman($pmuuid);
        
        $this->load->view('header',$this->header);
        $this->load->view('administrasi/pengumuman/pengumumanInput_vw',$data);
        $this->load->view('footer');
    }
    
    public function Edit($pmuuid) {
        $this->header["btnsave"] = btnsave('');
        $this->header["btnback"] = btnback(base_url()."administrasi/pengumuman/pengumuman");
        
        $data["govId"] = $this->govId;
        $data["formAction"] = "editSave";
        $data["pmuuid"] = $pmuuid;
        $data["getpengumuman"] = $this->Pengumuman_md->get_pengumuman($pmuuid);        
        $this->load->view('header',$this->header);
        $this->load->view('administrasi/pengumuman/pengumumanInput_vw',$data);
        $this->load->view('footer');
    }
    
    public function Delete($pmuuid) {       
        $data["govId"] = $this->govId;
        $data["formAction"] = "addSave";
        $data["usid"] = "ADMIN";
        $this->Pengumuman_md->delete_pengumuman($data,$pmuuid);
        redirect('/administrasi/pengumuman/pengumuman');
    }
    
    public function addSave() {
        $data_s["pmgovid"] =  $this->govId;
        $data_s["pmastdt"] = $_POST["pmastdt"];
        $data_s["pmaendt"] = $_POST["pmaendt"];
        $data_s["pmasbjc"] = $_POST["pmasbjc"];
        $data_s["pmadesc"] = $_POST["pmadesc"];
        $data_s["pmaupdt"] = date("Ymd");
        $data_s["pmastat"] = '20';
        $data_s["usid"] = "ADMIN";
        //echo  date_format($_POST["pmastdt"],"mmdd");
        $this->Pengumuman_md->save_pengumuman($data_s);
        redirect('/administrasi/pengumuman/pengumuman');
    }
    
    public function editSave() {
        $data_s["pmgovid"] =  $this->govId;
        $data_s["pmastdt"] = $_POST["pmastdt"];
        $data_s["pmaendt"] = $_POST["pmaendt"];
        $data_s["pmasbjc"] = $_POST["pmasbjc"];
        $data_s["pmadesc"] = $_POST["pmadesc"];
        $data_s["pmuuid"] = $_POST["pmuuid"];
        $data_s["pmaupdt"] = date("Ymd");
        $data_s["pmastat"] = '20';
        $data_s["usid"] = "ADMIN";
        //echo  date_format($_POST["pmastdt"],"mmdd");
        $this->Pengumuman_md->update_pengumuman($data_s);
        redirect('/administrasi/pengumuman/pengumuman');
    }
    public function back() {
        redirect('/administrasi/pengumuman/pengumuman');
    }
}