<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Petugas extends CI_Controller {
    
    private $header;
    private $govId;
    private $appid;
    
    public function __construct() {
        parent::__construct();
        $this->load->model('administrasi/petugas/Petugas_md');
        $this->load->model('master/jabatan/Jabatan_md');
        $this->govId = "1";
        
        $this->Petugas_md->init($this->govId);
        
        $this->appid="RTRW";
        $this->code = "JABATN";
        $this->Jabatan_md->init($this->appid, $this->govId, $this->code);
        
        $this->header["title"] = "Petugas";
        $_SESSION["cntpetugas"] = $this->Petugas_md->count_petugas();
        $_SESSION["pageCount"] = ceil($_SESSION["cntpetugas"]/RECORD_LIMIT);
        $this->header["jspath"] = "administrasi/petugas/petugas.js";
    }
    
    private function getlist_petugas($page, $search) {
       
        $startpos = ($page-1)*RECORD_LIMIT ;
        return $this->Petugas_md->list_petugas($startpos, RECORD_LIMIT, $search);
    }
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {       
        //$govId = $this->session->userdata('govId');
        //jika blum login
        //if (trim($govId) !== "") {
        $this->header["btndel"] = btndel("");    
        
        $data["govId"] = $this->govId;
       $data["pageCount"] = $_SESSION["pageCount"];
        
        $page = 1;
        if (isset($_POST["search"])) {
            $search = $_POST["search"];
        } else {
            $search ="";
        }
        $data["lstpetugas"] = $this->getlist_petugas($page, $search );
        
        $this->load->view('header',$this->header);
        $this->load->view('administrasi/petugas/petugas_vw', $data);
        $this->load->view('footer');
        /*} else {
            redirect(base_url());
        }*/
    }
    
    public function page($page) {
        $this->header["btndel"] = btndel("");
        $data["pageCount"] = $_SESSION["pageCount"];
        if (isset($_POST["search"])) {
            $search = $_POST["search"];
        } else {
            $search ="";
        }
        $data["lstpetugas"] = $this->getlist_petugas($page, $search );
        $this->load->view('header',$this->header);
        $this->load->view('administrasi/petugas/petugas_vw', $data);
        $this->load->view('footer');
    }
    
    public function add() {
        $this->header["btnsave"] = btnsave("");
        $this->header["btnback"] = btnback(base_url()."/administrasi/petugas/petugas");
        
        $data["govId"] = $this->govId;
        $data["id"] = "";
        $data["formAction"] = "addSave";
        $data["lstjabatan"] = $this->Jabatan_md->list_jabatan(0, RECORD_LIMIT);
        
        $this->load->view('header',$this->header);
        $this->load->view('administrasi/petugas/petugasInput_vw',$data);
        $this->load->view('footer');
    }
    
    public function edit($id) {
        $this->header["btnsave"] = btnsave("");
        $this->header["btnback"] = btnback(base_url()."/administrasi/petugas/petugas");
        
        $data["govId"] = $this->govId;
        $data["id"] = $id;
        $data["formAction"] = "editSave";     
        $data["lstjabatan"] = $this->Jabatan_md->list_jabatan(0, RECORD_LIMIT);
        
        $data["dtlpetugas"] = $this->Petugas_md->get_petugas($id);
        $this->load->view('header',$this->header);
        $this->load->view('administrasi/petugas/petugasInput_vw',$data);
        $this->load->view('footer');
    }
    
    public function addSave() {
        $data["govId"] = $this->govId;
        $ofgovid = $_POST["ofgovid"];      
        $ofposid = $_POST["ofposid"];
        $ofprnik = trim($_POST["ofprnik"]);        
        $ofprnam = trim($_POST["ofprnam"]);
        $ofaddrs = trim($_POST["ofaddrs"]);
        $ofactiv = $_POST["ofactiv"];        
        $ofdatfr = trim($_POST["ofdatfr"]);        
        $ofdatto = trim($_POST["ofdatto"]);
       
        $ofcity = $_POST["ofcity"];
        $usid = "ADMIN";
        
        $this->Petugas_md->insert_petugas($ofposid, $ofprnik, $ofprnam, $ofaddrs, $ofcity, $ofdatfr, $ofdatto, $ofactiv, $usid);
        redirect('/administrasi/petugas/petugas');
    }
    
    public function editSave() {
        $data["govId"] = $this->govId;
        $id = $_POST["id"];
       
        $ofposid = $_POST["ofposid"];
        $ofprnik = trim($_POST["ofprnik"]);
        $ofprnam = trim($_POST["ofprnam"]);
        $ofaddrs = trim($_POST["ofaddrs"]);
        $ofactiv = $_POST["ofactiv"];
        $ofdatfr = trim($_POST["ofdatfr"]);        
        $ofdatto = trim($_POST["ofdatto"]);
        
        $ofcity = $_POST["ofcity"];
        $usid = "ADMIN";
        
        $this->Petugas_md->update_petugas($id, $ofposid, $ofprnik, $ofprnam, $ofaddrs, $ofcity, $ofdatfr, $ofdatto, $ofactiv, $usid); 
            redirect('/administrasi/petugas/petugas/show/'.$id);
    }
    
    public function show($id) {
        $this->header["btnadd"] = btnadd(base_url()."/administrasi/petugas/petugas/add");        
        $this->header["btnback"] = btnback(base_url()."/administrasi/petugas/petugas");
        
        $data["govId"] = $this->govId;
        $data["id"] = $id;
               
        $data["dtlpetugas"] = $this->Petugas_md->get_petugas($id);
        $this->load->view('header',$this->header);
        $this->load->view('administrasi/petugas/petugasShow_vw',$data);
        $this->load->view('footer');
    }
    
    public function delete($id) {
       
        $this->Petugas_md->delete_petugas($id);
        redirect('/administrasi/petugas/petugas');
    }
    
    public function deletemany() {
       
        $arr = $_POST["id"];
        foreach ($arr as $id) {
            $this->Petugas_md->delete_petugas($id);
        }
        redirect('/administrasi/petugas/petugas');
    }
    
}