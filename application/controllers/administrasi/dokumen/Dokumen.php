<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dokumen extends CI_Controller {
    private $header;
    private $govId;
    private $data;

	public function __construct() {
	    parent::__construct();
	    $this->load->model('administrasi/dokumen/Dokumen_md');
	    $this->govId = "RT0001";
	    $this->Dokumen_md->init($this->govId);
	    
	    $this->header["title"] = "Dokumen";
	    $_SESSION["cntDokumen"] = $this->Dokumen_md->count_dokumen();
	    $_SESSION["pageCount"] = ceil($_SESSION["cntDokumen"]/RECORD_LIMIT);
	    $this->header["jspath"] = "administrasi/dokumen/dokumen.js";
	    
	    $this->data["base_url"] = base_url(); 

	}
	private function getlist_dokumen($page, $search) {
	    
	    $startpos = ($page-1)*RECORD_LIMIT ;
	    return $this->Dokumen_md->list_dokumen($startpos, RECORD_LIMIT, $search);
	}
	public function index()
	{
	    $this->header["btndel"] = btndel('');
	    $this->header["btnadd"] = btnadd('');
	    $this->header["btnlinkadd"] = btnlinkadd(base_url()."administrasi/dokumen/dokumen/add", "Tambah Dokumen", "Tambah Dokumen");
	    
	    $this->data["govId"] = $this->govId;
	    $this->data["pageCount"] = $_SESSION["pageCount"];
	    
	    $page = 1;
	    if (isset($_POST["search"])) {
	        $search = $_POST["search"];
	    } else {
	        $search ="";
	    }
	    $this->data["lstDok"] = $this->getlist_dokumen($page, $search);
	    
	    $this->load->view('header',$this->header);
	    $this->load->view('administrasi/dokumen/dokumen_vw', $this->data);
	    $this->load->view('footer');
	}
	public function add(){
	    $this->header["btnsave"] = btnsave("");
	    $this->header["btnback"] = btnback("");
	    
	    $this->data["govId"] = $this->govId;
	    $this->data["rowid"] = '';
	    $this->data["formAction"] = "addSave";
	    $this->data["lstKatDok"] = $this->get_kategori_dokumen();
	    
	    $this->load->view('header',$this->header);
	    $this->load->view('administrasi/dokumen/dokumenInput_vw',$this->data);
	    $this->load->view('footer');
	}
	public function addSave(){
	    chmod('./files',0777);
	    $info = array();
	    $path = "files/".$this->govId."/dokumen/".date('Y')."/".date('m')."/";
	    echo $path;
	    if(!is_dir($path)){
	        mkdir($path,'0777');
	    }
// 	    $path = "./".$path;
// 	    $config['upload_path']     = $path;
// 	    $config['allowed_types']   = 'gif|jpg|png|doc|docx|xls|xlsx|ppt|pptx';
// 	    $config['max_size']        = 5120;
	    
// 	    $this->load->library('upload', $config, true);
	    
// 	    if ( ! $this->upload->do_upload('gdflnm')){
// 	        $info = array('error' => $this->upload->display_errors());
// 	    }else{
// 	        $info = array('upload_data' => $this->upload->data());
// 	    }
// 	    echo "<pre>";
// 	    print_r($info);
// 	    echo "</pre>";
	    
// 	    $gdtitl  = $this->input->post("cbtext");
// 	    $cbdesc  = $this->input->post("cbtext");
// 	    $usid = 'ADMIN';
	    
// 	    $this->Kategori_dokumen_md->insert_kategori_dokumen($this->entityId, $this->govId, $this->code, $cbtext, $cbdesc, $usid);
// 	    redirect('/master/kategori_dokumen/kategori_dokumen');
	}
	public function get_kategori_dokumen(){
	    $entityId = 'RTRW';
	    $code = "KATDOK";
	    
	    $this->load->model('master/kategori_dokumen/Kategori_dokumen_md');
	    $this->Kategori_dokumen_md->init($entityId, $this->govId, $code);
	    $totalKatDok = $this->Kategori_dokumen_md->count_kategori_dokumen();
	    $lstKatDok = $this->Kategori_dokumen_md->list_kategori_dokumen(0,$totalKatDok);
	    return $lstKatDok;
	    
	}
	
}
