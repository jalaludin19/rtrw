<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data extends CI_Controller {
    private $header;
    private $govId;
    private $daftar_id;
    
    public function __construct() {
        parent::__construct();
        $this->load->model('administrasi/daftarlain/Daftarlain_md');
        $this->load->model('administrasi/daftarlain/Data_md');
        $this->load->model('administrasi/daftarlain/Columns_md');
        $this->govId = "1";
        $this->Data_md->init($this->govId);
        $this->Columns_md->init($this->govId);
        
        $this->header["title"] = "Data Daftar";
        $_SESSION["cnt"] = $this->Data_md->count_data();
        $_SESSION["pageCount"] = ceil($_SESSION["cnt"]/RECORD_LIMIT);
        $this->header["jspath"] = "administrasi/daftarlain/data.js";
    }
    
    function _remap($param) {
        
        $param_offset=0;
        $params = array_slice($this->uri->rsegment_array(), $param_offset);
       
        if (count($params)==2) {
            $this->index($param);
        } else {
           
            if ($params[2]=="add") {
                $this->add($param);
            } elseif ($params[2]=="addSave") {
                $this->addSave($param);
                //echo "==".trim($_POST["lccolnm"]);
            } elseif ($params[2]=="edit") {
                $this->edit($param, $params[3]);
            } elseif ($params[2]=="editSave") {
                $this->editSave($param);
            }  elseif ($params[2]=="deletemany") {
                $this->deletemany($param);
            } elseif ($params[2]=="delete") {
                $this->delete($param, $params[3]);
            } 
        }
    }
    
    public function index($daftar_id) {
        $this->daftar_id = $daftar_id;
        $this->Daftarlain_md->init($this->govId);
        $this->Data_md->setListId($this->daftar_id);
        $this->Columns_md->setListId($this->daftar_id);
        
        $this->header["btndel"] = btndel("");
        $this->header["btnback"] = btnback(base_url()."administrasi/daftarlain/daftarlain");
        
        $data["govId"] = $this->govId;
        $data["pageCount"] = $_SESSION["pageCount"];
        $data["daftar_id"] = $this->daftar_id;
        
        $page = 1;
        if (isset($_POST["search"])) {
            $search = $_POST["search"];
        } else {
            $search ="";
        }
        
        $data["lstdaftar"] = $this->Daftarlain_md->get_daftarlainLstid($this->daftar_id);
        $data["lstcolumns"] = $this->getlist_columns();
        $data["lstdata"] = $this->getlist_data($page, $search );
        
        $this->load->view('header',$this->header);
        $this->load->view('administrasi/daftarlain/data_vw', $data);
        $this->load->view('footer');
    }
    
    private function getlist_columns() {        
        return $this->Columns_md->list_column("");
    }
    
    private function getlist_data($page, $search) {
        
        $startpos = ($page-1)*RECORD_LIMIT ;
        return $this->Data_md->list_data($startpos, RECORD_LIMIT, $search);
    }
    
    public function add($daftar_id) {
        $this->daftar_id = $daftar_id;
        $this->Data_md->setListId($this->daftar_id);
        $this->Columns_md->setListId($this->daftar_id);
        
        $this->header["btnsave"] = btnsave("");
        $this->header["btnback"] = btnback(base_url()."administrasi/daftarlain/data/".$this->daftar_id);
        
        $data["govId"] = $this->govId;
        $data["id"] = "";
        $data["daftar_id"] = $this->daftar_id;
        $data["formAction"] = "addSave";
        $data["lstcolumns"] = $this->getlist_columns();
        $data["dtldata"] = null;
        
        $this->load->view('header',$this->header);
        $this->load->view('administrasi/daftarlain/dataInput_vw',$data);
        $this->load->view('footer');
    }
    
    public function addSave($daftar_id) {
        $this->daftar_id = $daftar_id;
        $this->Data_md->setListId($this->daftar_id);
        $this->Columns_md->setListId($this->daftar_id);
        
        $usid = "ADMIN";
        $datId = $this->Data_md->new_dtid();
        $lstcolumns = $this->getlist_columns();
        foreach ($lstcolumns as $c) {
            $text = $_REQUEST["col_".$c->lccolid];    
            $this->Data_md->insert_data($datId, $c->lccolid, $text, $usid);
        }
        //$this->Columns_md->insert_column($colname);
        redirect("/administrasi/daftarlain/data/".$this->daftar_id);
    }
    
    public function edit($daftar_id, $id) {
        $this->daftar_id = $daftar_id;
        $this->Data_md->setListId($this->daftar_id);
        $this->Columns_md->setListId($this->daftar_id);
      
        $this->header["btnsave"] = btnsave("");
        $this->header["btnback"] = btnback(base_url()."administrasi/daftarlain/data/".$this->daftar_id);
        
        $data["govId"] = $this->govId;
        $data["id"] = $id;
        $data["daftar_id"] = $this->daftar_id;
        $data["formAction"] = "editSave";
        
        $data["lstcolumns"] = $this->getlist_columns();
        $data["dtldata"] = $this->Data_md->get_data($id);
        $this->load->view('header',$this->header);
        $this->load->view('administrasi/daftarlain/dataInput_vw',$data);
        $this->load->view('footer');
    }
    
    public function editSave($daftar_id) {
        $this->daftar_id = $daftar_id;
        $this->Data_md->setListId($this->daftar_id);
        $this->Columns_md->setListId($this->daftar_id);
        
        $data["govId"] = $this->govId;
        $id = $_POST["id"];
        $usid = "ADMIN";
        
        $lstcolumns = $this->getlist_columns();
        foreach ($lstcolumns as $c) {
            $text = $_REQUEST["col_".$c->lccolid];
            $this->Data_md->update_data($id, $c->lccolid, $text, $usid);
        }
        
        redirect("/administrasi/daftarlain/data/".$this->daftar_id);
    }
    
    public function deletemany($daftar_id) {
       
        $this->daftar_id = $daftar_id;
        $this->Data_md->setListId($this->daftar_id);
        $this->Columns_md->setListId($this->daftar_id);
       
        $arr = $_POST["id"];
        
        foreach ($arr as $i) {
            
            $this->Data_md->delete_data($i);
        }
       
        redirect('/administrasi/daftarlain/data/'.$this->daftar_id);
    }
    
    public function delete($daftar_id, $id) {
        
        $this->daftar_id = $daftar_id;
        $this->Data_md->setListId($this->daftar_id);
        $this->Columns_md->setListId($this->daftar_id);
        
        $this->Data_md->delete_data($id);        
        
        redirect('/administrasi/daftarlain/data/'.$this->daftar_id);
    }
    
}