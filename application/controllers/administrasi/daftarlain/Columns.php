<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Columns extends CI_Controller {
    private $header;
    private $govId;
    private $daftar_id;
    
    public function __construct() {
        parent::__construct();
        $this->load->model('administrasi/daftarlain/Columns_md');
        $this->govId = "1";
        $this->Columns_md->init($this->govId);
        
        $this->header["title"] = "Atur Kolom Daftar Lain";
       
        $this->header["jspath"] = "administrasi/daftarlain/columns.js";
    }
    
    function _remap($param) {
       
        $param_offset=0;
        $params = array_slice($this->uri->rsegment_array(), $param_offset);
       
        if (count($params)==2) {
            $this->index($param);
        } else {
            
            if ($params[2]=="add") {
                $this->add($param);
            } elseif ($params[2]=="addSave") {
                $this->addSave($param);
                //echo "==".trim($_POST["lccolnm"]);
            } elseif ($params[2]=="edit") {
                $this->edit($param, $params[3]);
            } elseif ($params[2]=="editSave") {
                $this->editSave($param);
            } elseif ($params[2]=="deletemany") {
                $this->deletemany($param);
            }
        }
    }
    
    public function index($daftar_id) {
        $this->daftar_id = $daftar_id;
        $this->Columns_md->setListId($this->daftar_id);
        
        $this->header["btndel"] = btndel("");
        $this->header["btnback"] = btnback(base_url()."administrasi/daftarlain/daftarlain");
        
        $data["govId"] = $this->govId;
        $data["pageCount"] = $_SESSION["pageCount"];
        $data["daftar_id"] = $this->daftar_id;
        
        $page = 1;
        if (isset($_POST["search"])) {
            $search = $_POST["search"];
        } else {
            $search ="";
        }
        $data["lstcolumns"] = $this->getlist_columns($search );
        
        $this->load->view('header',$this->header);
        $this->load->view('administrasi/daftarlain/columns_vw', $data);
        $this->load->view('footer');
    }
    
    private function getlist_columns($search) {        
       
        return $this->Columns_md->list_column($search);
    }
    
    public function add($daftar_id) {
        $this->daftar_id = $daftar_id;
        $this->header["btnsave"] = btnsave("");
        $this->header["btnback"] = btnback(base_url()."administrasi/daftarlain/columns/".$this->daftar_id);
        
        $data["govId"] = $this->govId;
        $data["id"] = "";
        $data["daftar_id"] = $this->daftar_id;
        $data["formAction"] = "addSave";
       
        $this->load->view('header',$this->header);
        $this->load->view('administrasi/daftarlain/columnsInput_vw',$data);
        $this->load->view('footer');
    }
    
    public function addSave($daftar_id) {
        $this->daftar_id = $daftar_id;
        $this->Columns_md->setListId($this->daftar_id);
        
        $colname = trim($_POST["lccolnm"]);
      
        $this->Columns_md->insert_column($colname);
        redirect("/administrasi/daftarlain/columns/".$this->daftar_id);
    }
    
    public function edit($daftar_id, $id) {
        $this->daftar_id = $daftar_id;
        $this->header["btnsave"] = btnsave("");
        $this->header["btnback"] = btnback(base_url()."administrasi/daftarlain/columns/".$this->daftar_id);
        
        $data["govId"] = $this->govId;
        $data["id"] = $id;
        $data["daftar_id"] = $this->daftar_id;
        $data["formAction"] = "editSave";
        
        
        $data["dtlcolumn"] = $this->Columns_md->get_column($id);
        $this->load->view('header',$this->header);
        $this->load->view('administrasi/daftarlain/columnsInput_vw',$data);
        $this->load->view('footer');
    }
    
    public function editSave($daftar_id) {
        $this->daftar_id = $daftar_id;
        $data["govId"] = $this->govId;
        $id = $_POST["id"];
        
        $colname = trim($_POST["lccolnm"]);
        $this->Columns_md->update_column($id, $colname);
        redirect("/administrasi/daftarlain/columns/".$this->daftar_id);
    }
    
    public function deletemany($daftar_id) {
        $this->daftar_id = $daftar_id;
        $arr = $_POST["id"];
        foreach ($arr as $id) {
            $this->Columns_md->delete_column($id);
        }
        redirect('/administrasi/daftarlain/columns/'.$this->daftar_id);
    }
}