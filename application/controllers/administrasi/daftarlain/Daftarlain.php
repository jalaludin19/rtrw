<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftarlain extends CI_Controller {
    private $header;
    private $govId;
    
    public function __construct() {
        parent::__construct();
        $this->load->model('administrasi/daftarlain/Daftarlain_md');
        $this->load->model('administrasi/daftarlain/Data_md');
        $this->govId = "1";
        
        $this->Daftarlain_md->init($this->govId);
        $this->Data_md->init($this->govId);
        
        $this->header["title"] = "Daftar Lain-lain";
        $_SESSION["cnt"] = $this->Daftarlain_md->count_daftarlain();
        $_SESSION["pageCount"] = ceil($_SESSION["cnt"]/RECORD_LIMIT);
        $this->header["jspath"] = "administrasi/daftarlain/daftarlain.js";
    }
    
    private function getlist_daftarlain($page, $search) {
        
        $startpos = ($page-1)*RECORD_LIMIT ;
        return $this->Daftarlain_md->list_daftarlain($startpos, RECORD_LIMIT, $search);
    }
    
    public function index()
    {
        //$govId = $this->session->userdata('govId');
        //jika blum login
        //if (trim($govId) !== "") {
        $this->header["btndel"] = btndel("");
        
        $data["govId"] = $this->govId;
        $data["pageCount"] = $_SESSION["pageCount"];
        
        $page = 1;
        if (isset($_POST["search"])) {
            $search = $_POST["search"];
        } else {
            $search ="";
        }
        $data["lstdaftar"] = $this->getlist_daftarlain($page, $search );
        
        $this->load->view('header',$this->header);
        $this->load->view('administrasi/daftarlain/daftarlain_vw', $data);
        $this->load->view('footer');
        /*} else {
         redirect(base_url());
         }*/
    }
    
    public function page($page) {
        $this->header["btndel"] = btndel("");
        $data["pageCount"] = $_SESSION["pageCount"];
        if (isset($_POST["search"])) {
            $search = $_POST["search"];
        } else {
            $search ="";
        }
        $data["lstpetugas"] = $this->getlist_daftarlain($page, $search );
        $this->load->view('header',$this->header);
        $this->load->view('administrasi/daftarlain/daftarlain_vw', $data);
        $this->load->view('footer');
    }
    
    public function add() {
        $this->header["btnsave"] = btnsave("");
        $this->header["btnback"] = btnback(base_url()."administrasi/daftarlain/daftarlain");
        
        $data["govId"] = $this->govId;
        $data["id"] = "";
        $data["formAction"] = "addSave";
        
        $this->load->view('header',$this->header);
        $this->load->view('administrasi/daftarlain/daftarlainInput_vw',$data);
        $this->load->view('footer');
    }
    
    public function edit($id) {
        $this->header["btnsave"] = btnsave("");
        $this->header["btnback"] = btnback(base_url()."administrasi/daftarlain/daftarlain");
        
        $data["govId"] = $this->govId;
        $data["id"] = $id;
        $data["formAction"] = "editSave";
        
        
        $data["dtldaftar"] = $this->Daftarlain_md->get_daftarlain($id);
        $this->load->view('header',$this->header);
        $this->load->view('administrasi/daftarlain/daftarlainInput_vw',$data);
        $this->load->view('footer');
    }
    
    public function addSave() {
       
        $lsgovid = $_POST["lsgovid"];
        $lslname = trim($_POST["lslname"]);
        $lsdescr = trim($_POST["lsdescr"]);        
        $usid = "ADMIN";
        
        $this->Daftarlain_md->insert_daftarlain($lslname, $lsdescr, $usid);
        redirect('/administrasi/daftarlain/daftarlain');
    }
    
    public function editSave() {
        $data["govId"] = $this->govId;
        $id = $_POST["id"];
        
        $lslname = trim($_POST["lslname"]);
        $lsdescr = trim($_POST["lsdescr"]);
         $usid = "ADMIN";
        
         $this->Daftarlain_md->update_daftarlain($id, $lslname, $lsdescr, $usid );
        redirect('/administrasi/daftarlain/daftarlain');
    }
    
    public function delete($id) {
        $this->Data_md->setListId($id);
        $cnt = $this->Data_md->count_data($id);
        
        if ($cnt == 0) {
            $this->Daftarlain_md->delete_daftarlain($id);         
            
        }
        redirect('/administrasi/daftarlain/daftarlain');
    }
    
    public function deletemany() {
        
        $arr = $_POST["id"];
        foreach ($arr as $id) {
            $this->Data_md->setListId($id);
            $cnt = $this->Data_md->count_data($id);
            if ($cnt == 0) {
                $this->Daftarlain_md->delete_daftarlain($id);
            }
        }
        redirect('/administrasi/daftarlain/daftarlain');
    }
    
    
}