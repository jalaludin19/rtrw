<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Info_form extends CI_Controller {
    
	public function __construct() {
		parent::__construct();
		$this->load->model('profile/info/Info_md');
		//$this->load->helper('button_helper');
		
	}
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        $header["title"]       = "Profile Info";
        $header["btnsave"]     = btnsave('');
    	$header["btnedit"]     = btnedit('profile/info/info_vw_edit');
    	$header["jspath"]      = "profile/info/info_form.js";
    	$header["frmaction"]   = "info_form";
    	
    	$header["listprov"] = $this->Info_md->get_provinces();
    	
    	//get data kabupaten
    	
    	$header["listkab"]  = $this->Info_md->get_regencies($this->input->post('cbprov'));
    	
    	
    	//get data kecamatan
    	
    	$header["listkec"]  = $this->Info_md->get_district($this->input->post('cbkab'));
    	
    	
    	//get data kelurahan
    	
    	 $header["listkel"]  = $this->Info_md->get_villages($this->input->post('cbkec'));
    	
    	
    	if ($this->input->post('mode')=="add"){
    	    
    	    //parameter to input
    	    $data =array(
    	        "GPPRVID"=> $this->input->post('cbprov'),
    	        "GPRGCID"=> $this->input->post('cbkab'),
    	        "GPDISID"=> $this->input->post('cbkec'),
    	        "GPVILID"=> $this->input->post('cbkel'),
    	        "GPRTNUM"=> $this->input->post('gprtnum'),
    	        "GPRWNUM"=> $this->input->post('gprwnum'),
    	        "GPADDRS"=> $this->input->post('gpaddrs'),
				"GPPOSCD"=> $this->input->post('gpposcd'),
				"GPPHONE"=> $this->input->post('gpphone'),
				"GPEMAIL"=> $this->input->post('gpemail'),
				"GPRTNM"=> 	$this->input->post('gprtnm'),
				"GPRWNM"=> 	$this->input->post('gprwnm'),
    	        "GPFLNM"=>  $_FILES['gplogo']['name']
    	        
    	    );
    	    $this->save($data);
    	}
    	
    	
    	$header["cbprov"] = $this->input->post('cbprov');
    	$header["cbkab"]  = $this->input->post('cbkab');
    	$header["cbkec"]  = $this->input->post('cbkec');
    	$header["gpaddrs"]= $this->input->post('gpaddrs');
    	$header["gprtnum"]= $this->input->post('gprtnum');
    	
    	$header["data_info"] = $this->Info_md->getProfileInfo(1);
		
        $this->load->view('header',$header);
        $this->load->view('profile/info/info_vw_edit');
        $this->load->view('footer');
    }
    
   
    
    public function save($data){
       
        $this->load->model('profile/info/Info_md');
        $this->Info_md->save_profile($data);
        $filename 		= $_FILES['gplogo']['name'];
        
        $filepath   	= "assets/images/";
        $file           = $filepath.$filename;
        if(!file_exists($filepath)) {
            mkdir($filepath,0777,true);
            
            @move_uploaded_file($_FILES['gplogo']['tmp_name'], $file);
        }
        else{
            @move_uploaded_file($_FILES['gplogo']['tmp_name'], $file);
        }
       
       
        
    }
    
    public function edit($data){
        
    }
    
    
    
}