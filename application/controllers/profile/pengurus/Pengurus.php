<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengurus extends CI_Controller {
    
    private $header;
    private $govId;
    private $appid;
    
    public function __construct() {
        parent::__construct();
        $this->load->model('profile/pengurus/Pengurus_md');
        $this->load->model('master/jabatan/Jabatan_md');
        $this->govId = "1";
        
        $this->Pengurus_md->init($this->govId);
        
        $this->appid="RTRW";
        $this->code = "JABATN";
        $this->Jabatan_md->init($this->appid, $this->govId, $this->code);
        
        $this->header["title"] = "Pengurus";
        $_SESSION["cntpengurus"] = $this->Pengurus_md->count_pengurus();
        $_SESSION["pageCount"] = ceil($_SESSION["cntpengurus"]/RECORD_LIMIT);
        $this->header["jspath"] = "profile/pengurus/pengurus.js";
    }
    
    private function getlist_pengurus($page, $search) {
       
        $startpos = ($page-1)*RECORD_LIMIT ;
        return $this->Pengurus_md->list_pengurus($startpos, RECORD_LIMIT, $search);
    }
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {       
        //$govId = $this->session->userdata('govId');
        //jika blum login
        //if (trim($govId) !== "") {
        $this->header["btndel"] = btndel("");    
        
        $data["govId"] = $this->govId;
       $data["pageCount"] = $_SESSION["pageCount"];
        
        $page = 1;
        if (isset($_POST["search"])) {
            $search = $_POST["search"];
        } else {
            $search ="";
        }
        $data["lstpengurus"] = $this->getlist_pengurus($page, $search );
        
        $this->load->view('header',$this->header);
        $this->load->view('profile/pengurus/pengurus_vw', $data);
        $this->load->view('footer');
        /*} else {
            redirect(base_url());
        }*/
    }
    
    public function page($page) {
        $this->header["btndel"] = btndel("");
        $data["pageCount"] = $_SESSION["pageCount"];
        if (isset($_POST["search"])) {
            $search = $_POST["search"];
        } else {
            $search ="";
        }
        $data["lstpengurus"] = $this->getlist_pengurus($page, $search );
        $this->load->view('header',$this->header);
        $this->load->view('profile/pengurus/pengurus_vw', $data);
        $this->load->view('footer');
    }
    
    public function add() {
        $this->header["btnsave"] = btnsave("");
        $this->header["btnback"] = btnback(base_url()."/profile/pengurus/pengurus");
        
        $data["govId"] = $this->govId;
        $data["id"] = "";
        $data["formAction"] = "addSave";
        $data["lstjabatan"] = $this->Jabatan_md->list_jabatan(0, RECORD_LIMIT);
        
        $this->load->view('header',$this->header);
        $this->load->view('profile/pengurus/pengurusInput_vw',$data);
        $this->load->view('footer');
    }
    
    public function edit($id) {
        $this->header["btnsave"] = btnsave("");
        $this->header["btnback"] = btnback(base_url()."/profile/pengurus/pengurus");
        
        $data["govId"] = $this->govId;
        $data["id"] = $id;
        $data["formAction"] = "editSave";     
        $data["lstjabatan"] = $this->Jabatan_md->list_jabatan(0, RECORD_LIMIT);
        
        $data["dtlpengurus"] = $this->Pengurus_md->get_pengurus($id);
        $this->load->view('header',$this->header);
        $this->load->view('profile/pengurus/pengurusInput_vw',$data);
        $this->load->view('footer');
    }
    
    public function addSave() {
        $data["govId"] = $this->govId;
        $mggovid = $_POST["mggovid"];      
        $mgposid = $_POST["mgposid"];
        $mgprnik = trim($_POST["mgprnik"]);        
        $mgprnam = trim($_POST["mgprnam"]);
        $mgaddrs = trim($_POST["mgaddrs"]);
        $mgactiv = $_POST["mgactiv"];        
        $mgdatfr = trim($_POST["mgdatfr"]);        
        $mgdatto = trim($_POST["mgdatto"]);
       
        $mgcity = $_POST["mgcity"];
        $usid = "ADMIN";
        
        $this->Pengurus_md->insert_pengurus($mgposid, $mgprnik, $mgprnam, $mgaddrs, $mgcity, $mgdatfr, $mgdatto, $mgactiv, $usid);
        redirect('/profile/pengurus/pengurus');
    }
    
    public function editSave() {
        $data["govId"] = $this->govId;
        $id = $_POST["id"];
       
        $mgposid = $_POST["mgposid"];
        $mgprnik = trim($_POST["mgprnik"]);
        $mgprnam = trim($_POST["mgprnam"]);
        $mgaddrs = trim($_POST["mgaddrs"]);
        $mgactiv = $_POST["mgactiv"];
        $mgdatfr = trim($_POST["mgdatfr"]);        
        $mgdatto = trim($_POST["mgdatto"]);
        
        $mgcity = $_POST["mgcity"];
        $usid = "ADMIN";
        
        $this->Pengurus_md->update_pengurus($id, $mgposid, $mgprnik, $mgprnam, $mgaddrs, $mgcity, $mgdatfr, $mgdatto, $mgactiv, $usid); 
            redirect('/profile/pengurus/pengurus/show/'.$id);
    }
    
    public function show($id) {
        $this->header["btnadd"] = btnadd(base_url()."/profile/pengurus/pengurus/add");        
        $this->header["btnback"] = btnback(base_url()."/profile/pengurus/pengurus");
        
        $data["govId"] = $this->govId;
        $data["id"] = $id;
               
        $data["dtlpengurus"] = $this->Pengurus_md->get_pengurus($id);
        $this->load->view('header',$this->header);
        $this->load->view('profile/pengurus/pengurusShow_vw',$data);
        $this->load->view('footer');
    }
    
    public function delete($id) {
       
        $this->Pengurus_md->delete_pengurus($id);
        redirect('/profile/pengurus/pengurus');
    }
    
    public function deletemany() {
       
        $arr = $_POST["id"];
        foreach ($arr as $id) {
            $this->Pengurus_md->delete_pengurus($id);
        }
        redirect('/profile/pengurus/pengurus');
    }
    
}